# Servizio Sanità Ansebadeca

## Funzionalità
#### Paziente
* E' assegnato ad un medico di base da cui si reca per farsi visitare
* Può consultare la lista di esami fatti e da prenotare
* Può prenotare presso un medico specialista o un SSP un esame che gli è stato prescritto
* Può pagare il ticket dell'esame da prenotare in fase di prenotazione
* Può controllare la lista delle ricette prescritte
* Può stampare la ricetta e recarsi in farmacia per ottenere il farmaco
* Può cambiare il suo medico di base con un medico della sua stessa provincia
* Riceve una mail quando gli viene fornita una prescrizione
* Può cambiare la propria foto
* Può cambiare il proprio medico di base
#### Medico di Base
* Vede il proprio parco pazienti
* Visualizza per ogni paziente le proprie prescrizioni di farmaci, esami e visite effettuate
* Prescrive esami e farmaci ad un paziente
* Emette una visita al paziente
#### Medico Specialista
* Vede la lista di pazienti iscritti al servizio sanitario
* Visualizza per ogni paziente le proprie prescrizioni di farmaci, esami e visite effettuate
* Vede la lista di esami da effettuare in giornata odierna
* Esegue esami ed emette una visita contente l'esito dell'esame ed eventuali raccomandazioni per il paziente
#### Servizio Sanitario Provinciale
* Vede la lista di pazienti iscritti al servizio sanitario
* Visualizza per ogni paziente le proprie prescrizioni di farmaci, esami e visite effettuate
* Vede la lista di esami da effettuare in giornata odierna
* Esegue esami ed emette una visita contente l'esito dell'esame ed eventuali raccomandazioni per il paziente
* Può stampare in formato xls la lista delle ricette con data di prescrizione della propria provincia

## Requisiti

Per il corretto funzionamento del progetto è necessario inserire la cartella sl.db che contiene il database utilizzato nel seguente percorso

```
D:/DataBaseWeb/sl.db
```

Se utilizzate un sistema linux o volete specifare un altro percorso per il database è necessario modificare nel file web.xml il parametro dburl (riga 5)


## Built With

* [Java](https://www.java.com) - Linguaggio di programmazione
* [Maven](https://maven.apache.org/) -  Strumento di gestione di progetti software basati su Java 
* [Tomcat](http://tomcat.apache.org/) - Web server open source
* JSP - Tecnologia di programmazione Web in Java
* HTML - Linguaggio di markup
* Javascript - Linguaggio di scripting

> Java Dependencies

| Dependency                                                                                            | Description                                                                                                                                                                                                                |
| ----------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Java Servlet](https://www.oracle.com/technetwork/java/index-jsp-135475.html)                         | Java servlets can respond to many types of requests, they most commonly implement web containers for hosting web applications on web servers and thus qualify as a server-side servlet web API                             |                                                        |
| [Javax Mail](https://mvnrepository.com/artifact/javax.mail)                                             | Libreria per la creazione e l'invio di email                                                                                                                                                                      |
| [iText](https://itextpdf.com/en)                                             | Libreria per la creazione e manipulazione di PDF                                                                                                                                                                     |
| [Google ZXing QR Code](https://www.callicoder.com/generate-qr-code-in-java-using-zxing/)              | Libreria per la creazione di QR                                                                                                                                                                           | |

> JavaScript Dependencies

| Dependency                                                                                            | Description                                                                                                                                                                                                                |
| ----------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Bootstrap](https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css)                                             | Kit open source per utilizzare HTML, CSS e JS in modo responsive                                                                                                                                                                     |
| [Ajax](https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js)                                             | Libreria JS per la gestione di chiamate asincrone in modo da aggiornare una porzione di pagine per volta                                                                                                                                                                     |
| [jQuery](https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js)              | Libreria JS arricchita per la manipolazione del documento e gestione eventi                                                                                                                                                                            |
| [DataTable](https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css)              | Strumento per la creazione (abbellimento) di tabelle HTML tramite JS                                                                                                                                                                           |
| [Select2](https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css)              | Oggetto jQuery per rimpiazzare la select box                                                                                                                                                                            |  |

## Authors

* **Jacopo Donà** - Matricola: 194032 - email: jacopo.dona@studenti.unitn.it
* **Michele Rigo** - Matricola: 195244 - email: michele.rigo-1@studenti.unitn.it
* **Matteo Lovato** - Matricola: 191870 - email: matteo.lovato-1@studenti.unitn.it


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

