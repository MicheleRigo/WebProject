<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.concurrent.ThreadLocalRandom"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.time.LocalDate"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.ParcoPazientiDAO"%>
<%@page import="java.util.LinkedList"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.CambiaMedicoDiBaseDAO"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Prescrizione"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrescrizioniDAO"%>
<%@page import="com.sun.org.apache.xml.internal.security.utils.IdResolver"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente"%>
<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    private UserDAO dao;
    private PrescrizioniDAO prescrizioniDAO;
    private CambiaMedicoDiBaseDAO cambiaMedicoDiBaseDAO;
    private ParcoPazientiDAO parcoPazientiDAO;

    public void jspInit() {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system"));
        }
        try {
            dao = daoFactory.getDAO(UserDAO.class);
            prescrizioniDAO = daoFactory.getDAO(PrescrizioniDAO.class);
            cambiaMedicoDiBaseDAO = daoFactory.getDAO(CambiaMedicoDiBaseDAO.class);
            parcoPazientiDAO = daoFactory.getDAO(ParcoPazientiDAO.class);
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
    }

    public void jspDestroy() {
        if (dao != null) {
            dao = null;
        }

        if (prescrizioniDAO != null) {
            prescrizioniDAO = null;
        }

        if (cambiaMedicoDiBaseDAO != null) {
            cambiaMedicoDiBaseDAO = null;
        }

        if (parcoPazientiDAO != null) {
            parcoPazientiDAO = null;
        }
    }
%>
<%

    Paziente authenticatedUser = (Paziente) request.getSession(false).getAttribute("user");

    String cp = getServletContext().getContextPath();
    if (!cp.endsWith("/")) {
        cp += "/";
    }

    String avatarsFolder = getServletContext().getInitParameter("avatarsFolder");
    if (avatarsFolder == null) {
        System.err.println("non trovata pdfFolder");
    }

    avatarsFolder = getServletContext().getRealPath(avatarsFolder);

    String avatarPath = "images/avatars/" + authenticatedUser.getFoto();
    //String avatarPath = avatarsFolder+"/"+authenticatedUser.getFoto();
    //String avatarPath= "images/avatars/Immagini.jpg";
    final String contextPath = cp;


%>
<!DOCTYPE html>
<html>
    <head>
        <title><%=authenticatedUser.getNome().substring(0, 1).toUpperCase() + authenticatedUser.getNome().substring(1).toLowerCase()%> 
            <%=authenticatedUser.getCognome().substring(0, 1).toUpperCase() + authenticatedUser.getCognome().substring(1).toLowerCase()%>
        </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <style>
            #barra_menu > li > a{
                color: #fff;
            }
            #barra_menu > li > a.active{
                color: #5cb85c;
            }

        </style>
    </head>


    <body>
        <!- Inizio arra di ricerca ->

        <div class="jumbotron jumbotron-fluid bg-success text-white " style="margin-bottom:0">

            <div class="container" style="margin-left:0px">
                <div class="row">
                    <div class="col-sm-2" >
                        <img id="immagineProfilo" src="<%=avatarPath%>"  width="150" height="150" class="img-responsive rounded">
                    </div>
                    <div class="col-sm-8">
                        <h1><% if (authenticatedUser.getSesso().equals("M")) {%>
                            Benvenuto 
                            <%} else {%>
                            Benvenuta 
                            <%}%>

                            <%=authenticatedUser.getNome().substring(0, 1).toUpperCase() + authenticatedUser.getNome().substring(1).toLowerCase()%></h1>      

                    </div>

                            <div class="col-sm-1" ><a href="logout.handler" class="btn btn-success">Logout</a></div>
                      

                </div>
            </div>
        </div>

        <ul id = "barra_menu" class="nav nav-tabs nav-justified sticky-top bg-success" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#Prescrizioni"><h6>Prescrizioni</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#Esami"><h6>Esami</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#LeMieVisite"><h6>Le mie visite</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#InfoPers"><h6>Informazioni Personali</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " id="CambiaMedicoTab" data-toggle="tab" href="#CambiaMedico"><h6>Cambia Medico Di Base</h6></a>
            </li>
        </ul>
        <!- Fine barra di ricerca->

        <div class="tab-content">
            <!-- Prescrizioni -->
            <div id="Prescrizioni" class="container tab-pane active">
                <br>
                <br>
                <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                    <h2>Prescrizioni Esami</h2>
                    <br>
                    <table id="EsamiTable" class="table table-hover">
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td>Data</td>
                                <td>Medico</td>
                                <td>Descrizione</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>

                <br>
                <br>
                <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                    <h2>Prescrizioni    Farmaci</h2>
                    <br>
                    <table id="FarmaciTable" class="table table-hover" >
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td>Data</td>
                                <td>Medico</td>
                                <td>Descrizione</td>
                                <td></td>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- Fine Prescrizioni -->

            <!-- Esami -->
            <div id="Esami" class="container tab-pane fade">

                <br>
                <br>
                <div id="jumbooo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                    <h2>Prenotazioni</h2>
                    <br>
                    <table id="prenotazioniEsamiTable" class="table table-hover display" width="100%">
                        <thead>
                            <tr>
                                <td>Esame</td>
                                <td>Data</td>
                                <td>Ora</td>
                                <td>Costo</td>
                                <td>Pagato</td>
                                <td>Medico</td>

                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                    <br>
                    <a class="btn btn-success text-white " id="reportTiket" ><h6>Report Spese Ticket</h6></a>

                </div>
                <br>
                <div id="jumbooo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                    <h2>Esami Effettuati</h2>
                    <br>
                    <table id="esamiEffettuatiTable" class="table table-hover display" width="100%">
                        <thead>
                            <tr>
                                <td>Nome Medico</td>
                                <td>Esame</td>
                                <td>Esito</td>
                                <td>Data</td>

                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Fine Esami Effettuati -->

            <!-- Le mie visite -->
            <div id="LeMieVisite" class="container tab-pane fade">
                <br>
                <br>
                <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                    <h2>Visite Medico di Base</h2>
                    <br>
                    <table id="visiteMedicoDiBaseTable" class="table table-hover display" width="100%">
                        <thead>
                            <tr>
                                <td>Data</td>
                                <td>Descrizione</td>
                                <td>Nome Medico</td>
                                <td>Cognome Medico  </td>

                            </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
                <br><br>

            </div>
            <!-- Le mie visite -->

            <!-- Informazioni Personali -->
            <div id="InfoPers" class="container tab-pane fade">
                <br>
                <br>
                <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                    <h2>Profilo</h2>
                    <br>
                    <table class="table table-hover display" width="100%">
                        <thead>
                        </thead>

                        <tbody>
                            <tr>
                                <td>Foto</td>
                                <td><button class="btn btn-success btn-sm" data-toggle="modal" data-target="#CambiaFotoModal" >Cambia Foto</button></td>
                            </tr>
                            <tr>
                                <td>Nome</td>
                                <td><%= authenticatedUser.getNome().substring(0, 1).toUpperCase() + authenticatedUser.getNome().substring(1).toLowerCase()%></td>
                            </tr>
                            <tr>
                                <td>Cognome</td>
                                <td><%= authenticatedUser.getCognome().substring(0, 1).toUpperCase() + authenticatedUser.getCognome().substring(1).toLowerCase()%></td>
                            </tr>
                            <tr>
                                <td>Data Di Nascita</td>
                                <td><%= authenticatedUser.getDataDiNascita()%></td>
                            </tr>
                            <tr>
                                <td>Sesso</td>
                                <td><%= authenticatedUser.getSesso()%></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td><button id="btn-changetab" class="btn btn-success btn-sm" onclick="" data-toggle="modal" data-target="#CambiaPasswordModal" >Cambia Password</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded">
                    <h2>Informazioni di contatto</h2>
                    <br>
                    <table class="table table-hover display" width="100%">
                        <thead>
                        </thead>

                        <tbody>
                            <tr>
                                <td>E-Mail</td>
                                <td><%= authenticatedUser.getEmail()%></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Fine Informazioni Personali -->

            <!-- Cambia Medico -->
            <div id="CambiaMedico" class="container tab-pane fade">
                <br>
                <br>
                <div  class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                    <h2>Seleziona uno dei seguenti medici</h2>
                    <br>
                    <p>Il tuo attuale medico di base è: <b id="NomeCognomeMedicoCorrente"></b></p><br>



                    <table id="CambiaMedicoTable" class="table table-sm table-hover table-striped display" width="100%">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Cognome</th>
                                <th>Via</th>
                                <th>Civico</th>
                                <th>Cap</th>
                                <th>Provincia</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>      
            </div>
            <!-- Fine Cambia Medico -->
        </div>    

        <!-- Modal Cambia Medico Di Base -->
        <div class="modal" id="myModal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Stai per cambiare il tuo medico di base</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Sei sicuro di voler cambiare il tuo medico di base con: <b><div id="labelCambiaMedico"></div></b>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Annulla</button>
                        <form action="cambiaMedicoDiBase.handler" method="POST">
                            <div style="display:none;">
                                <input id="nomeMed" name="nomeMed" class="form-control">
                                <input id="cognomeMed" name="cognomeMed" class="form-control">
                                <input id="viaMed" name="viaMed" class="form-control">
                                <input id="civicoMed" name="civicoMed" class="form-control">
                                <input id="capMed" name="capMed" class="form-control">
                                <input id="provinciaMed" name="provinciaMed" class="form-control">
                                <input id="idUtente" name="idUtente" class="form-control">
                            </div>
                            <div style="display:none;">

                            </div>
                            <button class="btn btn-success" type="submit" >Conferma</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>


        <!-- Modal Cambia Password -->
        <div class="modal fade" id="CambiaPasswordModal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Cambio Password</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <form  action="cambiapassword.handler" method="POST">
                        <div class="modal-body">
                            Inserisci la nuova password
                            <input type="password" id="password1" name="password1" class="form-control" placeholder="Password" required>
                            <br>
                            Reinserisci la nuova password
                            <input type="password" id="password2" name="password2" class="form-control" placeholder="Password" required>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button  class="btn btn-danger" data-dismiss="modal">Annulla</button>
                            <button  class="btn btn-success" type="submit">Conferma</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal Cambia Foto-->

        <div class="modal fade" id="CambiaFotoModal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Cambia Foto</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <br>

                    <!-- Modal body -->
                    <div class="modal-body" >
                        <input type="hidden" name="idUser" id="idUser" class="form-control" value="<%=authenticatedUser.getId()%>">
                        <div class="form-label-group">
                            <input type="file" name="avatar" id="avatar" class="form-control" placeholder="Avatar" value="<%=avatarPath%>" >
                            <label for="avatar">Avatar</label>

                        </div>

                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button id="uploadFile" class="btn btn-success">Cambia</button>

                    </div>

                </div>
            </div>
        </div>


        <!-- Modal Prenota Esame-->
        <div class="modal fade" id="PrenotaEsame">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Prenota Esame</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->

                    <div class="modal-body container">
                        <div id="inserisciData" > 
                            <b>Inserisci la data</b>
                            <input class="form-control" id="dataPrenotazione" type="text" placeholder="yyyy-mm-dd" title="format : yyyy-mm-dd"/>
                            <br>
                            <b>Inserisci l'ora</b>
                            <input class="form-control" id="oraOrenotazione" type="text" placeholder="hh:mm" title="format : hh:mm"/>
                        </div>
                        <div id="decidiSpecialistaSSP" class="container" style="display:none;">
                            <b>Decidi da chi farti fare l'esame</b><br>
                            <center>
                                <button  id="sceltoSSP" class="btn btn-success" type="submit" >SSP</button>
                                <button  id="sceltoSpecialista" class="btn btn-success" type="submit" >Medico Specialista</button>
                            </center>
                        </div>
                        <div id="pagamentoPrenotazione" style="display:none;">
                            <b id="bbbb">Prezzo: </b><label id="costo"></label><br>

                        </div>
                        <div id="scegliMedicoSpecialista" style="display:none;">
                            <table id="scegliMedicoSpecialistaTable" class="table table-hover">
                                <thead>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>Nome</td>
                                        <td>Cognome</td>
                                        <td>Via</td>
                                        <td>Numero Civico</td>
                                        <td></td>
                                        <td></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>







                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button  class="btn btn-danger esciPrenotazione" data-dismiss="modal">Annulla</button>
                        <button  id="confermaDate" class="btn btn-success" type="submit" >Conferma</button>
                        <button id="paga" class="btn btn-success" type="submit" style="display:none;">Paga e conferma</button>
                        <button id="continua" class="btn btn-success" type="submit" style="display:none;">Conferma</button

                    </div>

                </div>
            </div>
        </div>
    </div>





    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>
    <script src="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

    <!-- Javascript -->
    <script>
        var files;
        var fdata = new FormData();
        $("#uploadFile").on("click", function (e) {
            var fd = new FormData();
            var files = $('#avatar')[0].files[0];
            fd.append('avatar', files);
            fd.append('id', <%=authenticatedUser.getId()%>);

            $.ajax({
                url: 'cambiaFoto.handler',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#CambiaFotoModal').modal('hide');
                    $('#immagineProfilo').attr("src", 'images/avatars/' + response.path);

                }

            });




        });


        $(document).on("click", "#reportTiket", function () {
            $.get("pdf.handler", {idUtente:<%=authenticatedUser.getId()%>}, function (responseText) {
                window.open('pdfs/' + responseText.path);
            });
        });


        var cambiaMedicoTable;
        var isCambiaMedicoTableCreated = false;

        //Cambia Medico di Base
        $(document).on("click", "#CambiaMedicoTab", function () {

            $.get("cambiaMedicoDiBase.handler", {id: "<%=authenticatedUser.getId()%>", provincia: "<%=authenticatedUser.getProvincia()%>"}, function (responseText) {
                var string = responseText.medicoCorrente.nome.charAt(0).toUpperCase() + responseText.medicoCorrente.nome.slice(1) + " " + responseText.medicoCorrente.cognome.charAt(0).toUpperCase() + responseText.medicoCorrente.cognome.slice(1);
                $("#NomeCognomeMedicoCorrente").text(string);

                var oTblReport = $("#CambiaMedicoTable");
                if (isCambiaMedicoTableCreated) {
                    cambiaMedicoTable.destroy();
                }
                cambiaMedicoTable = oTblReport.DataTable({

                    "bInfo": false,
                    "ordering": false,
                    data: responseText.listaMedici,
                    "columns": [
                        {"data": "nome"},
                        {"data": "cognome"},
                        {"data": "viaStudio"},
                        {"data": "numeroCivicoStudio"},
                        {"data": "capStudio"},
                        {"data": "provincia"},
                        {"defaultContent": '<button class="btn btn-success btn-sm"  data-toggle="modal" href="#myModal">Seleziona</button>'}

                    ]

                });

                isCambiaMedicoTableCreated = true;

                $('#CambiaMedicoTable tbody').on('click', 'button', function () {
                    var dati = (cambiaMedicoTable.row($(this).parents('tr')).data());
                    //alert(dati.viaStudio +" <%= authenticatedUser.getId()%> "+ dati.numeroCivicoStudio+" "+ dati.capStudio+" "+dati.provincia );
                    document.getElementById('labelCambiaMedico').innerHTML = dati.nome.charAt(0).toUpperCase() + dati.nome.slice(1) + " " + dati.cognome.charAt(0).toUpperCase() + dati.cognome.slice(1);
                    document.getElementById('nomeMed').value = dati.nome;
                    document.getElementById('cognomeMed').value = dati.cognome;
                    document.getElementById('viaMed').value = dati.viaStudio;
                    document.getElementById('civicoMed').value = dati.numeroCivicoStudio;
                    document.getElementById('capMed').value = dati.capStudio;
                    document.getElementById('provinciaMed').value = dati.provincia;
                    document.getElementById('idUtente').value = <%= authenticatedUser.getId()%>;

                });
            });
        });

        var tablePrescrizioniFarmaci;
        var tablePrescrizioniEsami;
        var tableVisitaMedicoDiBase;
        var tablescegliMedicoSpecialista;
        var tableEsami;
        var oTblReport5 = $("#scegliMedicoSpecialistaTable");

        var tablePrenotazioni;
        var oTblReport6 = $("#prenotazioniEsamiTable");
        var oTblReport2 = $("#EsamiTable");

        function refreshPrenotazioni() {
            //alert("refresh");
            $.get("getDatiPaziente.handler", {"id": <%=authenticatedUser.getId()%>}, function (responseText1) {
                tablePrenotazioni = oTblReport6.DataTable({
                    destroy: true,
                    "bInfo": false,
                    data: responseText1.listaPrenotazioni,
                    "columns": [
                        {"data": "nomeEsame"},
                        {"data": "dataEsame"},
                        {"data": "oraEsame"},
                        {"data": "costo"},
                        {"data": "pagato"},
                        {"data": "pagato"}
                    ]
                });
                tablePrescrizioniEsami = oTblReport2.DataTable({
                    destroy: true,
                    "bInfo": false,
                    data: responseText.listaEsami,
                    "columns": [
                        {"data": "esame"},
                        {"data": "dataPrescrizione"},
                        {"data": "nomeMedico"},
                        {"data": "descrizione"},
                        {"data": "id",
                            "visible": false},
                        {"data": "specializzazioneEsame",
                            "visible": false},
                        {"data": "idEsame",
                            "visible": false},
                        {"data": "prenotato",
                            "visible": false},
                        {"data": null},
                                //{"defaultContent":'<center><div class="btn-group"><button class="btn btn-success btn-sm stampaEsame"><img src="images/print.png" class="img-responsive" width="20" height="20"></button><button id="sel" class="btn btn-success btn-sm prenotaBottone" data-toggle="modal" data-target="#PrenotaEsame">Prenota</button></div></center>'}
                    ],

                    columnDefs: [{
                            // puts a button in the last column
                            targets: [-1], render: function (a, b, data, d) {
                                //alert("iiiin");
                                //alert(data.prenotato);
                                if (data.prenotato) {

                                    return '<center><div class="btn-group"><button class="btn btn-success btn-sm stampaEsame"><img src="images/print.png" class="img-responsive" width="20" height="20"></button><button id="sel" class="btn btn-success btn-sm prenotaBottone" disabled>Prenotato</button></div></center>';
                                }
                                return '<center><div class="btn-group"><button class="btn btn-success btn-sm stampaEsame"><img src="images/print.png" class="img-responsive" width="20" height="20"></button><button id="sel" class="btn btn-success btn-sm prenotaBottone" data-toggle="modal" data-target="#PrenotaEsame">Prenota</button></div></center>';
                            }
                        }],
                });
            });
        }

        //Tabelle prescrizioni, VisitaMedicoDiBase e VisitaSpecialistica     
        $.get("getDatiPaziente.handler", {"id": <%=authenticatedUser.getId()%>}, function (responseText) {

            tablePrenotazioni = oTblReport6.DataTable({
                destroy: true,
                "bInfo": false,
                data: responseText.listaPrenotazioni,
                "columns": [
                    {"data": "nomeEsame"},
                    {"data": "dataEsame"},
                    {"data": "oraEsame"},
                    {"data": "costo"},
                    {"data": "pagato"},
                    {"data": "nomeMedico"}


                ]
            });

            //Tabella Prescrizione Farmaci
            var oTblReport = $("#FarmaciTable");

            tablePrescrizioniFarmaci = oTblReport.DataTable({
                destroy: true,
                "bInfo": false,
                data: responseText.listaFarmaci,
                "columns": [
                    {"data": "farmaco"},
                    {"data": "dataPrescrizione"},
                    {"data": "nomeMedico"},
                    {"data": "descrizione"},
                    {"defaultContent": '<center><button class="btn btn-success btn-sm stampaFarmaco" onclick="" ><img src="images/print.png" class="img-responsive" width="20" height="20"></button></center>'}
                    //
                    //button class="btn btn-success btn-sm" onclick="location.href = 'restricted/export2PDF';" ><img src="images/print.png" class="img-responsive" width="20" height="20"></button>
                ]
            });

            //Tabella Prescrizione Esami


            tablePrescrizioniEsami = oTblReport2.DataTable({
                destroy: true,
                "bInfo": false,
                data: responseText.listaEsami,
                "columns": [
                    {"data": "esame"},
                    {"data": "dataPrescrizione"},
                    {"data": "nomeMedico"},
                    {"data": "descrizione"},
                    {"data": "id",
                        "visible": false},
                    {"data": "specializzazioneEsame",
                        "visible": false},
                    {"data": "idEsame",
                        "visible": false},
                    {"data": "prenotato",
                        "visible": false},
                    {"data": null},
                            //{"defaultContent":'<center><div class="btn-group"><button class="btn btn-success btn-sm stampaEsame"><img src="images/print.png" class="img-responsive" width="20" height="20"></button><button id="sel" class="btn btn-success btn-sm prenotaBottone" data-toggle="modal" data-target="#PrenotaEsame">Prenota</button></div></center>'}
                ],

                columnDefs: [{
                        // puts a button in the last column
                        targets: [-1], render: function (a, b, data, d) {
                            //alert("iiiin");
                            //alert(data.prenotato);
                            if (data.prenotato) {

                                return '<center><div class="btn-group"><button id="sel" class="btn btn-success btn-sm prenotaBottone" disabled>Prenotato</button></div></center>';
                            }
                            return '<center><div class="btn-group"><button id="sel" class="btn btn-success btn-sm prenotaBottone" data-toggle="modal" data-target="#PrenotaEsame">Prenota</button></div></center>';
                        }
                    }],
            });



            //Tabella Visita MedicoDiBase
            var oTblReport3 = $("#visiteMedicoDiBaseTable");

            tableVisitaMedicoDiBase = oTblReport3.DataTable({
                destroy: true,
                "bInfo": false,
                data: responseText.listaVisiteMedicoDiBase,
                "columns": [
                    {"data": "dataVisita"},
                    {"data": "descrizione"},
                    {"data": "nomeMedico"},
                    {"data": "cognomeMedico"}






                ]
            });


            var oTblReport7 = $("#esamiEffettuatiTable");

            tableEsami = oTblReport7.DataTable({
                destroy: true,
                "bInfo": false,

                data: responseText.listaVisite,
                "columns": [
                    {"data": "nomeMedico"},
                    {"data": "nomeEsame"},
                    {"data": "esito"},
                    {"data": "data"}

                ]
            });



            $('#EsamiTable tbody').on('click', '.stampaEsame', function () {
                var dati = (tablePrescrizioniEsami.row($(this).parents('tr')).data());

                //alert("id prescrizione: " + dati.id);
                $.post("pdf.handler", {"id": dati.id}, function (responseText) {
                    window.open('./images/logo.png');
                });


            });

            $('#FarmaciTable tbody').on('click', '.stampaFarmaco', function () {
                var dati2 = (tablePrescrizioniFarmaci.row($(this).parents('tr')).data());
                //alert("id prescrizione: "+dati2.id);
                $.post("pdf.handler", {"id": dati2.id}, function (responseText) {
                    //window.open(""+responseText.path+"");
                    //alert('pdfs/'+responseText.path);
                    window.open('pdfs/' + responseText.path);
                });
            });

            $('#EsamiTable tbody').on('click', '.prenotaBottone', function () {
                var id = $(this).attr('id');
                //alert(id);




                document.getElementById('inserisciData').style.display = "block";
                document.getElementById('pagamentoPrenotazione').style.display = "none";

                document.getElementById('confermaDate').style.display = "block";
                document.getElementById('paga').style.display = "none";

                document.getElementById('decidiSpecialistaSSP').style.display = "none";
                document.getElementById('scegliMedicoSpecialista').style.display = "none";
                var rowtable = (tablePrescrizioniEsami.row($(this).parents('tr')).data());
                //alert("asdasd"+rowtable.specializzazioneEsame);

                $(document).on("click", "#confermaDate", function () {//quando l'utente conferma le date

                    if (document.getElementById('dataPrenotazione').value.charAt(4) != '-' && document.getElementById('dataPrenotazione').value.charAt(7) != '-') {
                        alert("Inserisci la data nel formato corretto!");
                    } else if (document.getElementById('oraOrenotazione').value.charAt(2) != ':') {
                        alert("Inserisci l'ora nel formato corretto!");
                    } else {
                        

                        //tolgo la pagina del seleziona data
                        document.getElementById('inserisciData').style.display = "none";
                        document.getElementById('confermaDate').style.display = "none";

                        var json;
                        //alert("specializzazione: " + rowtable.specializzazioneEsame);
                        $.get("mediciSpecialisti.handler", {"provincia": "<%=authenticatedUser.getProvincia()%>", "specializzazione": rowtable.specializzazioneEsame}, function (responseText) {
                            json = responseText.listaMediciSpecialista;
                            if (jQuery.isEmptyObject(json)) {
                                alert("Non ci sono specialisti disponibili per questo esame, verrà svolto nell SSP della tua provincia");
                            }
                            //json={"listaMediciSpecialista":[{"cognome":"bianchi","nome":"lucia","viaStudio":"via galileo galilei","numeroCivicoStudio":"11","id":3},{"cognome":"cocci","nome":"preudenzia","viaStudio":"via il bagno","numeroCivicoStudio":"22","id":16},{"cognome":"offredi","nome":"annamaria","viaStudio":"via al castello","numeroCivicoStudio":"6","id":20}]}
                            if (rowtable.specializzazioneEsame === 109 || jQuery.isEmptyObject(json)) {
                                document.getElementById('costo').innerHTML = "11$";

                                //manda a video il pagamento
                                document.getElementById('pagamentoPrenotazione').style.display = "block";
                                document.getElementById('paga').style.display = "block";

                                $(document).on("click", "#paga", function () {

                                    $('#PrenotaEsame').modal('hide');
                                    document.getElementById('inserisciData').style.display = "block";
                                    document.getElementById('pagamentoPrenotazione').style.display = "none";
                                    document.getElementById('confermaDate').style.display = "block";
                                    document.getElementById('paga').style.display = "none";
                                    //alert(document.getElementById('dataPrenotazione').value);
                                    //alert(document.getElementById('oraOrenotazione').value);
                                    $.post("prenotazioneEsame.handler", {"idPaziente": "<%=authenticatedUser.getId()%>",
                                        "idEsame": rowtable.idEsame,
                                        "dataEsame": document.getElementById('dataPrenotazione').value,
                                        "oraEsame": document.getElementById('oraOrenotazione').value,
                                        "idMedico": 0,
                                        "provincia": "<%=authenticatedUser.getProvincia()%>",
                                        "costo": 11,
                                        "idPrescrizione": rowtable.id,
                                        "pagato": true}, function () {
                                        $('[href="#Esami"]').tab('show');
                                        document.getElementById(id).disabled = true;
                                        document.getElementById(id).innerHTML = "Prenotato";

                                        refreshPrenotazioni();

                                    });

                                });

                            } else {
                                //manda a video il decidi medico
                                //alert("decidi specialista")
                                document.getElementById('decidiSpecialistaSSP').style.display = "block";

                                $(document).on("click", "#sceltoSSP", function () {
                                    //alert("cioa");
                                    document.getElementById('decidiSpecialistaSSP').style.display = "none";
                                    document.getElementById('costo').innerHTML = "11$";

                                    //manda a video il pagamento
                                    document.getElementById('pagamentoPrenotazione').style.display = "block";
                                    document.getElementById('paga').style.display = "block";

                                    $(document).on("click", "#paga", function () {
                                        $('#PrenotaEsame').modal('hide');
                                        document.getElementById('inserisciData').style.display = "block";
                                        document.getElementById('pagamentoPrenotazione').style.display = "none";
                                        document.getElementById('confermaDate').style.display = "block";
                                        document.getElementById('paga').style.display = "none";

                                        $.post("prenotazioneEsame.handler", {"idPaziente": "<%=authenticatedUser.getId()%>",
                                            "idEsame": rowtable.idEsame,
                                            "dataEsame": document.getElementById('dataPrenotazione').value,
                                            "oraEsame": document.getElementById('oraOrenotazione').value,
                                            "idMedico": 0,
                                            "provincia": "<%=authenticatedUser.getProvincia()%>",
                                            "costo": 11,
                                            "idPrescrizione": rowtable.id,
                                            "pagato": true}, function () {
                                            $('[href="#Esami"]').tab('show');
                                            document.getElementById(id).disabled = true;
                                            document.getElementById(id).innerHTML = "Prenotato";
                                            refreshPrenotazioni();
                                        });
                                    });
                                });


                                $(document).on("click", "#sceltoSpecialista", function () {

                                    document.getElementById('decidiSpecialistaSSP').style.display = "none";
                                    document.getElementById('scegliMedicoSpecialista').style.display = "block";

                                    $.get("mediciSpecialisti.handler", {"provincia": "<%=authenticatedUser.getProvincia()%>", "specializzazione": rowtable.specializzazioneEsame}, function (responseText) {
                                        //alert("sto facendo la tabella")
                                        //Tabella Prescrizione Farmaci




                                        tablescegliMedicoSpecialista = oTblReport5.DataTable({
                                            destroy: true,
                                            "bInfo": false,
                                            data: responseText.listaMediciSpecialista
                                            ,
                                            "columns": [
                                                {"data": "nome"},
                                                {"data": "cognome"},
                                                {"data": "viaStudio"},
                                                {"data": "numeroCivicoStudio"},
                                                {"data": "id",
                                                    "visible": false},
                                                {"defaultContent": '<center><button class="btn btn-success btn-sm sceltoSpecialista" onclick="" >Seleziona</button></center>'}
                                                //
                                                //button class="btn btn-success btn-sm" onclick="location.href = 'restricted/export2PDF';" ><img src="images/print.png" class="img-responsive" width="20" height="20"></button>
                                            ]
                                        });
                                    });

                                    $(document).on("click", ".sceltoSpecialista", function () {

                                        var dati = (tablescegliMedicoSpecialista.row($(this).parents('tr')).data());


                                        document.getElementById('scegliMedicoSpecialista').style.display = "none";

                                        //alert("cioa");

                                        document.getElementById('costo').innerHTML = "50$";

                                        //manda a video il pagamento
                                        document.getElementById('pagamentoPrenotazione').style.display = "block";
                                        document.getElementById('paga').style.display = "block";

                                        $(document).on("click", "#paga", function () {
                                            $('#PrenotaEsame').modal('hide');
                                            document.getElementById('inserisciData').style.display = "block";
                                            document.getElementById('pagamentoPrenotazione').style.display = "none";
                                            document.getElementById('confermaDate').style.display = "block";
                                            document.getElementById('paga').style.display = "none";
                                            //tablescegliMedicoSpecialista.destroy();

                                            $.post("prenotazioneEsame.handler", {"idPaziente": "<%=authenticatedUser.getId()%>",
                                                "idEsame": rowtable.idEsame,
                                                "dataEsame": document.getElementById('dataPrenotazione').value,
                                                "oraEsame": document.getElementById('oraOrenotazione').value,
                                                "idMedico": dati.id,
                                                "provincia": "<%=authenticatedUser.getProvincia()%>",
                                                "costo": 50,
                                                "idPrescrizione": rowtable.id,
                                                "pagato": true}, function () {
                                                $('[href="#Esami"]').tab('show');
                                                document.getElementById(id).disabled = true;
                                                document.getElementById(id).innerHTML = "Prenotato";
                                                refreshPrenotazioni();
                                            });
                                        });
                                    });


                                });


                            }
                        });
                        //alert(json.nome);







                    }

                });


            });


        });




        $(document).on("click", ".esciPrenotazione", function () {
            document.getElementById('inserisciData').style.display = "block";
            document.getElementById('pagamentoPrenotazione').style.display = "none";

            document.getElementById('confermaDate').style.display = "block";
            document.getElementById('paga').style.display = "none";

            document.getElementById('decidiSpecialistaSSP').style.display = "none";
            document.getElementById('scegliMedicoSpecialista').style.display = "none";

            //tablescegliMedicoSpecialista.destroy();
        });
















        /*$(".custom-file-input").on("change", function () {
         var fileName = $(this).val().split("\\").pop();
         $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
         //alert(fileName);
         $.post("cambiaPassword.handler",{'file': 'filename'}, function(){
         
         });
         
         });
         */








    </script>

</body>
</html>
