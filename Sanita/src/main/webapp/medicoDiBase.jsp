<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.FarmaciDAO"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Farmaco"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.ParcoPazientiDAO"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc.JDBCParcoPazientiDAO"%>
<%@page import="java.util.LinkedList"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase"%>
<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    private UserDAO dao;
    private ParcoPazientiDAO parcoPazientiDAO;
    private FarmaciDAO farmaciDAO;

    public void jspInit() {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system"));
        }
        try {
            dao = daoFactory.getDAO(UserDAO.class);
            parcoPazientiDAO = daoFactory.getDAO(ParcoPazientiDAO.class);
            farmaciDAO = daoFactory.getDAO(FarmaciDAO.class);
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
    }

    public void jspDestroy() {
        if (dao != null) {
            dao = null;
        }
        if (parcoPazientiDAO != null) {
            parcoPazientiDAO = null;
        }
        if (farmaciDAO != null) {
            farmaciDAO = null;
        }
    }
%>
<%

    MedicoDiBase authenticatedUser = (MedicoDiBase) request.getSession(false).getAttribute("user");

    String cp = getServletContext().getContextPath();
    if (!cp.endsWith("/")) {
        cp += "/";
    }

    final String contextPath = cp;


%>
<!DOCTYPE html>
<html>
    <head>
        <title><%=authenticatedUser.getNome().substring(0, 1).toUpperCase() + authenticatedUser.getNome().substring(1).toLowerCase()%> 
            <%=authenticatedUser.getCognome().substring(0, 1).toUpperCase() + authenticatedUser.getCognome().substring(1).toLowerCase()%>
        </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
        
        <link rel="stylesheet" href="css/forms.css">
        <style>
            #barra_menu > li > a{
                color: #fff;
            }
            #barra_menu > li > a.active{
                color: #d9534f;
            }

            .autocomplete-items {
                border: 1px solid #d4d4d4;
                border-top: none;
                border-bottom: none;
            }

            .autocomplete-items div {
                padding: 10px;
                cursor: pointer;
                background-color: #fff;
                border-bottom: 1px solid #d4d4d4;
            }
            .autocomplete-items div:hover {
                background-color: #e9e9e9;
            }
            .autocomplete-active {
                /* usando le freccette */
                background-color: DodgerBlue !important;
                color: #ffffff;
            }
        </style>
    </head>


    <body>
        <!-- Inizio arra di ricerca -->

        <div class="jumbotron jumbotron-fluid bg-danger text-white " style="margin-bottom:0">

            <div class="container" style="margin-left:0px">
                <div class="row">
                    <div class="col-sm-2" >
                    </div>
                    <div class="col-sm-8">
                        <h1><% if (authenticatedUser.getSesso().equals("M")) {%>
                            Benvenuto 
                            <%} else {%>
                            Benvenuta 
                            <%}%>
                            <%=authenticatedUser.getNome().substring(0, 1).toUpperCase() + authenticatedUser.getNome().substring(1).toLowerCase()%></h1>      
                        <p>Sei un <%= authenticatedUser.tipoUtente()%></p>
                    </div>
                    <div class="col-sm-1" >
                        <a href="logout.handler" class="btn btn-danger">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <ul id = "barra_menu" class="nav nav-tabs nav-justified nav-item sticky-top bg-danger" role="tablist">
            <li class="nav-item">
                <a class="nav-link active " data-toggle="tab" href="#AA"><h6>Parco Pazienti</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#BB"><h6>Visita</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#DD"  ><h6>Riepilogo</h6></a>
            </li>
        </ul>

        <!-- Fine barra di ricerca-->



        <div class="container" style="margin-top:30px">   
            <div class="tab-content">

                <!-- Parco Pazienti -->
                <div id="AA" class="container tab-pane active">
                    <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                        <table id="tabellaPazienti" class="table table-sm table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Cognome</th>
                                    <th>Data Di Nascita</th>
                                    <th>Codice Fiscale</th>
                                    <th>id Utente</th>
                                    <th>Provincia</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nome</th>
                                    <th>Cognome</th>
                                    <th>Data Di Nascita</th>
                                    <th>Codice Fiscale</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>        
                </div>
                <!-- Fine Parco Pazienti -->

                <!-- Visita -->
                <div id="BB" class="container tab-pane fade">
                    <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                        <form class="form-label-group">
                            <h5>Paziente</h5><br>
                            <div class = "autocompletamento">
                                <input type="text" class="form-control" id="exampleFormControlInput1" autocomplete="off">
                            </div>
                            <br><br>
                            
                            <div class="jumbotron bg-white border border-grey">
                                <h5>Aggiungi Esami</h5><br>
                                    <div class="form-group">
                                        <select id="esami"  class="js-example-responsive select2-allow-clear" style="width: 100%"></select>
                                    </div>
                                <a id="addEsame" class="btn btn-lg btn-primary btn-block text-white">Aggiungi esame</a>
                                <table id="tabellaListaEsami" class="table table-sm table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br><br>
                            <div class="jumbotron bg-white border border-grey">
                                <h5>Aggiungi Farmaci</h5><br>

                                <div class=" mb-3">
                                    <div class="form-group">
                                    <select id="exampleFormControlInput3"  class="js-example-responsive select2-allow-clear" style="width: 100%"></select>
                                </div>
                                    <a id="addFarmaco" class="btn btn-lg btn-primary btn-block text-white">Aggiungi farmaco</a>
                                </div>
                                
                                <table id="tabellaListaFarmaci" class="table table-sm table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br><br>

                            <div class="form-group">
                                <h5>Descrizione</h5><br>
                                <textarea class="form-control" id="descrizione" rows="5" ></textarea>
                            </div>

                            <br>
                            <a id="riepilogo" class="btn btn-lg btn-primary btn-block text-white">Riepilogo</a>
                        </form>
                       
                    </div>
                </div>
                <!-- Fine Visita -->

                <!-- Riepilogo -->
                <div id="DD" class="container tab-pane fade">

                    <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                        <form class="form-label-group">    
                            
                            <h5>Paziente  </h5>
                            <div id="labelPaziente"></div>

                            <div id="divRiepilogoEsami">
                                <br><br>
                                <br>
                                <table id="riepilogoEsami" class="table table-sm table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th><h5>Esami</h5></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="divRiepilogoFarmaci">
                                <br><br>
                                <br>
                                <table id="riepilogoFarmaci" class="table table-sm table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th><h5>Farmaci</h5></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br><br>
                            <h5>Descrizione</h5><br>
                            <textarea class="form-control" id="descrizioneRiepilogo" rows="5" readonly="" ></textarea>
                            <br>
                            <button id="emettiVisita" class="btn btn-lg btn-primary btn-block text-white" type="button" >Emetti visita</button> 
                        </form>
                    </div>

                </div>
                <!-- Fine Riepilogo -->

            </div>

            <!-- Modal Dettagli Paziente -->
            <div class="modal" id="dettagliPaziente">
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h1 class="modal-title" id="nomeCognome"></h1>
                        <button id="" type="button" class="close tableDestroy" data-dismiss="modal">×</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="container">
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <img id="imgPaz" src=""  width="150" height="150" class="img-responsive rounded">
                                <h4><center><b>Dati Anagrafici</b></center></h4><br>
                                <table class="table table-hover display" width="100%">
                                    <thead>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>Nome</td>
                                            <td ><label id="nomePaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Cognome</td>
                                            <td ><label id="cognomePaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Data Di Nascita</td>
                                            <td ><label id="dataDiNascitaPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Luogo Di Nascita</td>
                                            <td ><label id="luogoDiNascitaPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Sesso</td>
                                            <td ><label id="sessoPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Codice Fiscale</td>
                                            <td ><label id="codiceFiscPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Residenza</td>
                                            <td ><label id="residenzaPaz"></label></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Prescrizioni Farmaci</b></center></h4><br>
                                <table id="tblReportResultsDemographics" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Nome</h6></th>
                                            <th><h6>Descrizione</h6></th>
                                            <th><h6>Data Prescrizione</h6></th>
                                            <th><h6>Nome Medico</h6></th>
                                            <th><h6>Cognome Medico</h6></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Prescrizioni Esami</b></center></h4><br>
                                <table id="prescrizioniEsami" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Nome</h6></th>
                                            <th><h6>Descrizione</h6></th>
                                            <th><h6>Data Prescrizione</h6></th>
                                            <th><h6>Nome Medico</h6></th>
                                            <th><h6>Cognome Medico</h6></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Visite Medico Di Base</b></center></h4><br>
                                <table id="visiteMedicoDiBase" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Nome Medico</h6></th>
                                            <th><h6>Cognome Medico</h6></th>
                                            <th><h6>Descrizione</h6></th>
                                            <th><h6>Data</h6></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Esami</b></center></h4><br>
                                <table id="visiteEsami" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Medico </h6></th>
                                            <th><h6>Esame</h6></th>
                                            <th><h6>Esito</h6></th>
                                            <th><h6>Data Esame</h6></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button id="" type="button" class="btn btn-danger tableDestroy" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
            <!-- Fine Modal Dettagli Paziente -->




            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>
        <script src="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

            <script>
                
                $("#esami").select2({
                    placeholder: 'Cerca Esame',
                    width: 'resolve',
                    allowClear: true,
                    ajax: {
                        url: "services/cercaEsami",
                        datatype: "json"
                    }
                });
                $("#esami").val(null).trigger("change");

                $("#exampleFormControlInput3").select2({
                    placeholder: 'Cerca Farmaco',
                    width: 'resolve',
                    allowClear: true,
                    ajax: {
                        url: "services/cercaFarmaci",
                        datatype: "json"
                    }
                });
                $("#exampleFormControlInput3").val(null).trigger("change");
                
                document.getElementById("emettiVisita").disabled = true;
                //Evento bottone emetti visita
                $('#emettiVisita').on('click', function(){
                                        
                    $('[href="#AA"]').tab('show');
                    //Prendi i dati e mandali alla servlet
                    
                    var str = document.getElementById("exampleFormControlInput1").value;
                    
                    var descrizione = document.getElementById("descrizioneRiepilogo").value;
                    
                    var codFisc = str.substring(str.length - 16, str.length);
                    var esami_prescritti = [];
                    var farmaci_prescritti = [];
                    
                          
                    tabellaEsami.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                        var data = this.data().toString().substring(125);
                        
                        esami_prescritti.push(data);
                        
                    });
                    
                    tabellaFarmaci.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                        var data = this.data().toString().substring(125);
                        
                        farmaci_prescritti.push(data);
                        
                    });
                    
                    for(var i = 0; i< farmaci_prescritti.length; i++){
                        $.post("emettiVisita.handler", {"codiceFiscalePaziente": codFisc,
                            "nome_farmaco": farmaci_prescritti[i], "nome_esame": "", 
                            "descrizione": descrizione, "id_medico": <%=authenticatedUser.getId()%>}, 
                            function (responseText) {}
                        );
                    }
                    
                    for(var i = 0; i< esami_prescritti.length; i++){
                        $.post("emettiVisitaEsami.handler", {"codiceFiscalePaziente": codFisc,
                            "nome_esame": esami_prescritti[i], "nome_farmaco": "", 
                            "descrizione": descrizione, "id_medico": <%=authenticatedUser.getId()%>}, 
                            function (responseText) {
                                console.log(responseText.errore);
                            }
                        );
                    }
                    
                    $.post("erogaVisita.handler", {"codiceFiscalePaziente": codFisc,
                            "descrizione": descrizione, "id_medico": <%=authenticatedUser.getId()%>}, 
                            function (responseText) {}
                        );
                    
                    
                    //pulisco tutti i campi
                    document.getElementById('exampleFormControlInput1').value = '';
                    document.getElementById('descrizione').value = '';
                    document.getElementById('descrizioneRiepilogo').value = '';
                    document.getElementById('labelPaziente').innerHTML = "";
                    riepilogoFarmaci.clear().draw();
                    riepilogoEsami.clear().draw();
                    tabellaFarmaci.clear().draw();
                    tabellaEsami.clear().draw();
                });
                
                var tablePazienti;
                //Tabella Parco Pazienti
                $.get("parcoPazienti.handler",{"id": <%=authenticatedUser.getId() %>}, function (responseText) {   
                         
                       
                        var oTblReport= $("#tabellaPazienti");

                        tablePazienti=oTblReport.DataTable ({
                            
                            "bInfo": false,
                            data : responseText.listaPazienti,
                            "columns" : [
                                 { "data" : "nome" },
                                 { "data" : "cognome" },
                                 { "data" : "dataDiNascita"},
                                 { "data" : "codiceFiscale" },
                                 { "data" : "id" },
                                 { "data" : "provincia" },
                                 { "data" : "id",
                                    "visible": false},
                                 {"defaultContent":'<center><div class="btn-group"></button><button id="btn" class="btn btn-danger btn-sm details">Seleziona</button><button id="btnDettagliPaziente" class="btn btn-danger btn-sm" data-toggle="modal" href="#dettagliPaziente" ><img src="images/piuInformazioni.png" height="20" width="20"></button></div></center>'}
                                 //
                                 //button class="btn btn-success btn-sm" onclick="location.href = 'restricted/export2PDF';" ><img src="images/print.png" class="img-responsive" width="20" height="20"></button>
                             ]
                        });
                         
                        
                        
                        $('#tabellaPazienti tbody').on('click', '.details', function () {
                            $('[href="#BB"]').tab('show');
                            var dati = (tablePazienti.row($(this).parents('tr')).data());
                            document.getElementById('exampleFormControlInput1').value = dati.nome + " " + dati.cognome + " " + dati.codiceFiscale;
                        });
                         
                         $('#tabellaPazienti tbody').on('click', '.stampaFarmaco', function () {
                            var dati = (tablePazienti.row($(this).parents('tr')).data());
                            alert("id prescrizione: "+dati.id);
                        });
                        
                        
                    });
                
                
                 
                //Tabelle Prescrizioni Dettagli Paziente
                var tablePrescrizioniFarmaci;
                var tablePrescrizioniEsami;
                $(document).on("click", "#btnDettagliPaziente", function () {
                    var dati = (tablePazienti.row($(this).parents('tr')).data());
            var string = dati.nome.charAt(0).toUpperCase() + dati.nome.slice(1) + " " + dati.cognome.charAt(0).toUpperCase() + dati.cognome.slice(1);
            $("#nomeCognome").text(string);

            $.get("getDatiPaziente.handler", {"id": dati.id}, function (responseText) {
                //alert(typeof responseText.listaEsami === 'object');
                //alert(responseText.listaEsami);
                $("#imgPaz").attr("src","images/avatars/"+responseText.paziente[0].foto);
                $("#nomePaz").text(responseText.paziente[0].nome);
                $("#cognomePaz").text(responseText.paziente[0].cognome);
                $("#dataDiNascitaPaz").text(responseText.paziente[0].dataDiNascita);
                $("#codiceFiscPaz").text(responseText.paziente[0].codiceFiscale);
                $("#sessoPaz").text(responseText.paziente[0].sesso);
                $("#luogoDiNascitaPaz").text(responseText.paziente[0].luogoDiNascita);
                $("#residenzaPaz").text(responseText.paziente[0].provincia);
                if (typeof responseText.listaEsami === 'object') {
                    //alert("in Esami");


                    var oTblReport = $("#tblReportResultsDemographics");

                    tablePrescrizioniFarmaci = oTblReport.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaFarmaci,
                        "columns": [
                            {"data": "farmaco"},
                            {"data": "descrizione"},
                            {"data": "dataPrescrizione"},
                            {"data": "nomeMedico"},
                            {"data": "cognomeMedico"}

                        ]
                    });
                }

                if (typeof responseText.listaFarmaci === 'object') {
                    //alert("in Farmaci");
                    var oTblReport2 = $("#prescrizioniEsami")

                    tablePrescrizioniEsami = oTblReport2.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaEsami,
                        "columns": [
                            {"data": "esame"},
                            {"data": "descrizione"},
                            {"data": "dataPrescrizione"},
                            {"data": "nomeMedico"},
                            {"data": "cognomeMedico"}

                        ]
                    });
                }

                if (typeof responseText.listaVisiteMedicoDiBase === 'object') {
                    //alert("in Esami");


                    var oTblReport3 = $("#visiteMedicoDiBase");

                    tableVisiteMedicoDiBase = oTblReport3.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaVisiteMedicoDiBase,
                        "columns": [
                            {"data": "nomeMedico"},
                            {"data": "cognomeMedico"},
                            {"data": "descrizione"},
                            {"data": "dataVisita"}

                        ]
                    });
                }

                if (typeof responseText.listaVisite === 'object') {
                    //alert("in Esami");


                    var oTblReport4 = $("#visiteEsami");

                    tablevisite = oTblReport4.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaVisite,
                        "columns": [
                            {"data": "nomeMedico"},
                            {"data": "nomeEsame"},
                            {"data": "esito"},
                            {"data": "data"}

                        ]
                    });
                }
            });
                    
                       
                        
                        
                         
                         
                    
                });
                
                $('.tableDestroy').on( 'click', function () {
                    tablePrescrizioniFarmaci.destroy();
                    tablePrescrizioniEsami.destroy();
                } );
                
                //Tabella Esami
                var tabellaEsami = $("#tabellaListaEsami").DataTable(
                        {paging: false,
                            searching: false,
                            "bInfo": false,
                            "ordering": false
                        }
                );
                document.getElementById('tabellaListaEsami').style.visibility = 'collapse';
                
                //Tabella Farmaci
                var tabellaFarmaci = $("#tabellaListaFarmaci").DataTable(
                        {paging: false,
                            searching: false,
                            "bInfo": false,
                            "ordering": false
                        }
                );
                document.getElementById('tabellaListaFarmaci').style.visibility = 'collapse';

                //Tabella riepilogo Esami
                var riepilogoEsami = $("#riepilogoEsami").DataTable(
                        {paging: false,
                            searching: false,
                            "bInfo": false,
                            "ordering": false
                        }
                );
        
                //Tabella Riepilogo Farmaci
                var riepilogoFarmaci = $("#riepilogoFarmaci").DataTable(
                        {paging: false,
                            searching: false,
                            "bInfo": false,
                            "ordering": false
                        }
                );


                var nomiList = [];
                var nomiFarmaci = [];
                var datiPazienti = [];
                
                //Prescrivi un esame al paziente
                $('#addEsame').on('click', function () {
                    if(!document.getElementById('esami').value == ""){
                        var e = document.getElementById("esami");
                        var strUser = e.options[e.selectedIndex].text;
                        tabellaEsami.row.add([
                            '<a class="btn btn-danger btn-sm btnrmv mr-4"><img src="images/2x/baseline_delete_white_18dp.png" height="20" width="20"></a>' + ' ' +
                            strUser

                        ]).draw(false);
                        document.getElementById('tabellaListaEsami').style.visibility = 'visible';
                    }
                    $('#esami').empty().trigger("change");
                });

                //Prescrivi un farmaco al paziente
                $('#addFarmaco').on('click', function () {
                    if(!document.getElementById('exampleFormControlInput3').value == ""){
                        var e = document.getElementById("exampleFormControlInput3");
                        var strUser = e.options[e.selectedIndex].text;
                        tabellaFarmaci.row.add([
                            '<a class="btn btn-danger btn-sm btnrmv mr-4"><img src="images/2x/baseline_delete_white_18dp.png" height="20" width="20"></a>' + ' ' +
                            strUser

                        ]).draw(false);
                        document.getElementById('tabellaListaFarmaci').style.visibility = 'visible';
                    }
                    $('#exampleFormControlInput3').empty().trigger("change");
                });

               
                //Bottone riepilogo
                $('#riepilogo').click(function () {
                    
                    riepilogoFarmaci.clear().draw();
                    riepilogoEsami.clear().draw();
                    if(document.getElementById('exampleFormControlInput1').value == ""){
                        alert("Inserisci il Paziente!");
                    }else{
                        document.getElementById("emettiVisita").disabled = false;
                        $('[href="#DD"]').tab('show');
                        document.getElementById('labelPaziente').innerHTML = document.getElementById('exampleFormControlInput1').value;

                        document.getElementById('descrizioneRiepilogo').innerHTML = document.getElementById('descrizione').value;

                        if (!tabellaFarmaci.data().count()) {
                            document.getElementById('divRiepilogoFarmaci').style.visibility = 'collapse';
                        } else {
                            document.getElementById('divRiepilogoFarmaci').style.visibility = 'visible';
                        }
                        if (!tabellaEsami.data().count()) {

                            document.getElementById('divRiepilogoEsami').style.visibility = 'collapse';
                        } else {
                            document.getElementById('divRiepilogoEsami').style.visibility = 'visible';
                        }
                        tabellaEsami.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                            var data = this.data().toString();
                            riepilogoEsami.row.add([
                            data
                        ]).draw(false);
                        });
                        tabellaFarmaci.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                            var data = this.data().toString();
                            riepilogoFarmaci.row.add([
                            data
                        ]).draw(false);
                        } );
                    }
                });
                
                //Elimina esame prescritto
                $('#tabellaListaEsami tbody').on('click', 'td', function () {
                    tabellaEsami.row($(this).closest('tr')).remove().draw(false);
                });
                
                //Elimina farmaco prescritto
                $('#tabellaListaFarmaci tbody').on('click', 'td', function () {
                    tabellaFarmaci.row($(this).closest('tr')).remove().draw(false);
                });
                
                //Elimina esame prescritto in riepilogo
                $('#riepilogoEsami tbody').on('click', 'td', function () {
                    riepilogoEsami.row($(this).closest('tr')).remove().draw(false);
                });
                
                //Elimina farmaco prescritto in riepilogo
                $('#riepilogoFarmaci tbody').on('click', 'td', function () {
                    riepilogoFarmaci.row($(this).closest('tr')).remove().draw(false);
                });
                
                
                

                $(function () {

                    $('#sendMailDialog').on('show.bs.modal', function (e) {
                        var button = $(e.relatedTarget);
                        var userId = button.data('user-id');
                        if (userId !== undefined) {
                            var userAddress = button.data('user-address');

                            $('#sendMailDialogLabel').html('Email to: ' + userAddress);
                            $('#userId').val(userId);
                        }
                    });

                    $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function () {
                        $(".alert-dismissible").alert('close');
                    });
                });
                
                //Prendo tutti i farmaci
                $.get("getFarmaciServlet.handler", function (responseText) {   
                    for(i in responseText.listaFarmaci)
                    {
                        nomiFarmaci[i] = responseText.listaFarmaci[i].nome;
                    }
                });
                function autocomplete(inp, arr) {
                    /*the autocomplete function takes two arguments,
                     the text field element and an array of possible autocompleted values:*/
                    var currentFocus;
                    /*execute a function when someone writes in the text field:*/
                    inp.addEventListener("input", function (e) {
                        var a, b, i, val = this.value;
                        /*close any already open lists of autocompleted values*/
                        closeAllLists();
                        if (!val) {
                            return false;
                        }
                        currentFocus = -1;
                        /*create a DIV element that will contain the items (values):*/
                        a = document.createElement("DIV");
                        a.setAttribute("id", this.id + "autocomplete-list");
                        a.setAttribute("class", "autocomplete-items");
                        /*append the DIV element as a child of the autocomplete container:*/
                        this.parentNode.appendChild(a);
                        /*for each item in the array...*/
                        for (i = 0; i < arr.length; i++) {
                            /*check if the item starts with the same letters as the text field value:*/
                            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                /*create a DIV element for each matching element:*/
                                b = document.createElement("DIV");
                                /*make the matching letters bold:*/
                                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                b.innerHTML += arr[i].substr(val.length);
                                /*insert a input field that will hold the current array item's value:*/
                                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                /*execute a function when someone clicks on the item value (DIV element):*/
                                b.addEventListener("click", function (e) {
                                    /*insert the value for the autocomplete text field:*/
                                    inp.value = this.getElementsByTagName("input")[0].value;
                                    /*close the list of autocompleted values,
                                     (or any other open lists of autocompleted values:*/
                                    closeAllLists();
                                });
                                a.appendChild(b);
                            }
                        }
                    });
                    /*execute a function presses a key on the keyboard:*/
                    inp.addEventListener("keydown", function (e) {
                        var x = document.getElementById(this.id + "autocomplete-list");
                        if (x)
                            x = x.getElementsByTagName("div");
                        if (e.keyCode == 40) {
                            /*If the arrow DOWN key is pressed,
                             increase the currentFocus variable:*/
                            currentFocus++;
                            /*and and make the current item more visible:*/
                            addActive(x);
                        } else if (e.keyCode == 38) { //up
                            /*If the arrow UP key is pressed,
                             decrease the currentFocus variable:*/
                            currentFocus--;
                            /*and and make the current item more visible:*/
                            addActive(x);
                        } else if (e.keyCode == 13) {
                            /*If the ENTER key is pressed, prevent the form from being submitted,*/
                            e.preventDefault();
                            if (currentFocus > -1) {
                                /*and simulate a click on the "active" item:*/
                                if (x)
                                    x[currentFocus].click();
                            }
                        }
                    });
                    function addActive(x) {
                        /*a function to classify an item as "active":*/
                        if (!x)
                            return false;
                        /*start by removing the "active" class on all items:*/
                        removeActive(x);
                        if (currentFocus >= x.length)
                            currentFocus = 0;
                        if (currentFocus < 0)
                            currentFocus = (x.length - 1);
                        /*add class "autocomplete-active":*/
                        x[currentFocus].classList.add("autocomplete-active");
                    }
                    function removeActive(x) {
                        /*a function to remove the "active" class from all autocomplete items:*/
                        for (var i = 0; i < x.length; i++) {
                            x[i].classList.remove("autocomplete-active");
                        }
                    }
                    function closeAllLists(elmnt) {
                        /*close all autocomplete lists in the document,
                         except the one passed as an argument:*/
                        var x = document.getElementsByClassName("autocomplete-items");
                        for (var i = 0; i < x.length; i++) {
                            if (elmnt != x[i] && elmnt != inp) {
                                x[i].parentNode.removeChild(x[i]);
                            }
                        }
                    }
                    /*execute a function when someone clicks in the document:*/
                    document.addEventListener("click", function (e) {
                        closeAllLists(e.target);
                    });
                }





               /* for (var i = 0; i < oTable.rows().count(); i++)
                {
                    var dati = oTable.row(i).data();
                    nomiList[i] = dati[0] + " " + dati[1] + " " + dati[3];
                }


                /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
                autocomplete(document.getElementById("exampleFormControlInput1"), nomiList);
            </script>

    </body>
</html>

