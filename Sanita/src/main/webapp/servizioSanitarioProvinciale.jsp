<%-- 
    Document   : servizioSanitarioRegionale
    Created on : 25 ott 2019, 17:35:55
    Author     : miche
--%>

<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ServizioSanitarioProvinciale"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente"%>
<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException"%>
<%@page import="it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory"%>
<%@page import="it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    private UserDAO dao;

    public void jspInit() {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system"));
        }
        try {
            dao = daoFactory.getDAO(UserDAO.class);
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
    }

    public void jspDestroy() {
        if (dao != null) {
            dao = null;
        }
    }
%>
<%

    ServizioSanitarioProvinciale authenticatedUser = (ServizioSanitarioProvinciale) request.getSession(false).getAttribute("user");

    String cp = getServletContext().getContextPath();
    if (!cp.endsWith("/")) {
        cp += "/";
    }

    final String contextPath = cp;


%>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Servizio Sanitario <%=authenticatedUser.getProvincia().substring(0, 1).toUpperCase() + authenticatedUser.getProvincia().substring(1).toLowerCase()%>
        </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
        <style>
            #barra_menu > li > a{
                color: #fff;
            }
            #barra_menu > li > a.active{
                color: #0275d8;
            }
        </style>
    </head>


    <body>
        <!- Inizio arra di ricerca ->

        <div class="jumbotron jumbotron-fluid bg-primary text-white " style="margin-bottom:0">

            <div class="container" style="margin-left:0px">
                <div class="row">
                    <div class="col-sm-2" >


                    </div>
                    <div class="col-sm-8">
                        <h1>
                            Servizio Sanitario 
                            <%=authenticatedUser.getProvincia().substring(0, 1).toUpperCase() + authenticatedUser.getProvincia().substring(1).toLowerCase()%>
                        </h1>
                    </div>
                    <div class="col-sm-2" >
                        <a href="logout.handler" class="btn btn-primary">Logout</a>
                    </div>
                </div>
            </div>

        </div>



        <ul id = "barra_menu" class="nav nav-tabs nav-justified sticky-top bg-primary" role="tablist">
            <li class="nav-item">
                <a class="nav-link active " data-toggle="tab" href="#AA"><h6>Pazienti</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#BB2"><h6>Esami di oggi</h6></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#BB"><h6>Esame</h6></a>
            </li>
            <li class="nav-item">
                <a class="btn btn-primary " id="reportXls" ><h6>Report xls</h6></a>
            </li>

        </ul>

        <!- Fine barra di ricerca->

        <div class="container" style="margin-top:30px">   
            <div class="tab-content">

                <!-- Parco Pazienti -->
                <div id="AA" class="container tab-pane active">
                    <br>
                    <br>
                    <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                        <h2>Pazienti</h2>
                        <br>
                        <table id="pazientiTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <td>Nome</td>
                                    <td>Cognome</td>
                                    <td>CodiceFiscale</td>
                                    <td>Data di nascita</td>
                                    <td></td>


                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>

                    </div>

                </div>
                <div id="BB2" class="container tab-pane">
                    <br>
                    <br>
                    <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                        <br>

                        <h2>Esami di oggi</h2>
                        <br>
                        <table id="esamiDiOggiTable" class="table table-hover" width="100%">
                            <thead>
                                <tr>
                                    <td>Nome Paziente</td>
                                    <td>Cognome Paziente</td>
                                    <td>Data di nascita</td>
                                    <td>Esame</td>
                                    <td>Data Esame</td>
                                    <td>Ora Esame</td>


                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>

                        <center><label id="nessunEsame">Non hai nessun esame oggi</label></center>

                    </div>
                </div>
                <div id="BB" class="container tab-pane">
                    <br>
                    <br>
                    <div id="jumbo" class="jumbotron bg-white border border-grey shadow p-3 mb-5 bg-white rounded" >
                        <center><h2>Esame</h2></center>
                        <br>
                        <form>

                            <br>
                            <div class="form-group">
                                <label><b>Paziente</b></label>
                                <select id="paziente"  class="js-example-responsive select2-allow-clear" style="width: 100%"></select>
                            </div>

                            <br>
                            <div class="form-group">
                                <label><b>Esami</b></label>
                                <select id="esami"  class="js-example-responsive select2-allow-clear" style="width: 100%"></select>
                            </div>
                            <br><br>
                            <h6><b>Esito:</b></h6>
                            <textarea class="form-control" id="esito" rows="5" ></textarea>

                            <br>
                            <a id="emettiEsame" class="btn btn-lg btn-primary btn-block text-white">Emetti esame</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal" id="dettagliPaziente">
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h1 class="modal-title" id="nomeCognome"></h1>
                        <button id="" type="button" class="close tableDestroy" data-dismiss="modal">×</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="container">
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <img id="imgPaz" src=""  width="150" height="150" class="img-responsive rounded">
                                <h4><center><b>Dati Anagrafici</b></center></h4><br>
                                <table class="table table-hover display" width="100%">
                                    <thead>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>Nome</td>
                                            <td ><label id="nomePaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Cognome</td>
                                            <td ><label id="cognomePaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Data Di Nascita</td>
                                            <td ><label id="dataDiNascitaPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Luogo Di Nascita</td>
                                            <td ><label id="luogoDiNascitaPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Sesso</td>
                                            <td ><label id="sessoPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Codice Fiscale</td>
                                            <td ><label id="codiceFiscPaz"></label></td>
                                        </tr>
                                        <tr>
                                            <td>Residenza</td>
                                            <td ><label id="residenzaPaz"></label></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Prescrizioni Farmaci</b></center></h4><br>
                                <table id="tblReportResultsDemographics" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Nome</h6></th>
                                            <th><h6>Descrizione</h6></th>
                                            <th><h6>Data Prescrizione</h6></th>
                                            <th><h6>Nome Medico</h6></th>
                                            <th><h6>Cognome Medico</h6></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Prescrizioni Esami</b></center></h4><br>
                                <table id="prescrizioniEsami" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Nome</h6></th>
                                            <th><h6>Descrizione</h6></th>
                                            <th><h6>Data Prescrizione</h6></th>
                                            <th><h6>Nome Medico</h6></th>
                                            <th><h6>Cognome Medico</h6></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Visite Medico Di Base</b></center></h4><br>
                                <table id="visiteMedicoDiBase" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Nome Medico</h6></th>
                                            <th><h6>Cognome Medico</h6></th>
                                            <th><h6>Descrizione</h6></th>
                                            <th><h6>Data</h6></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div id="jumbo" class="jumbotron bg-white border border-grey bg-white rounded" >
                                <h4><center><b>Esami</b></center></h4><br>
                                <table id="visiteEsami" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th><h6>Medico </h6></th>
                                            <th><h6>Esame</h6></th>
                                            <th><h6>Esito</h6></th>
                                            <th><h6>Data Esame</h6></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button id="" type="button" class="btn btn-danger tableDestroy" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal" id="visitaErogata">
            <div class="modal-dialog modal-dialog-centered modal-sm">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">

                        <button id="" type="button" class="close " data-dismiss="modal">×</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="container">
                            <center><p><b>Esame erogato con successo!</b></p></center>
                        </div>


                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button id="" type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>

                </div>
            </div>
        </div>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>
        <script src="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>




    </body>
    <script>
        
        $(document).on("click", "#reportXls", function (){
            
            $.get("xls.handler",{"provincia": '<%=authenticatedUser.getProvincia() %>'}, function (responseText){
                //alert(responseText.path);
                window.open('xls/'+responseText.path);
                
            });
        });
        
        
        var tableEsamiDiOggi;
        $.get("esamiDiOggiSSP.handler", {"idMedico": <%=authenticatedUser.getId()%>}, function (responseText) {
            
            
            if (typeof responseText.listaEsamiDiOggi[0] === 'object') {
                

                document.getElementById('nessunEsame').style.display = "none";

                var oTblRepor = $("#esamiDiOggiTable");

                tableEsamiDiOggi = oTblRepor.DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    "bInfo": false,

                    data: responseText.listaEsamiDiOggi,
                    "columns": [
                        {"data": "nomePaziente"},
                        {"data": "cognomePaziente"},
                        {"data": "dataDiNascitaPaziente"},
                        {"data": "nomeEsame"},
                        {"data": "dataEsame"},
                        {"data": "oraEsame"}

                    ]
                });
            } else {

                document.getElementById('esamiDiOggiTable').style.display = "none";
            }
        });



        $("#paziente").select2({
            placeholder: 'Cerca Paziente',
            width: 'resolve',
            allowClear: true,
            ajax: {
                url: "services/cercaPazienti2",
                datatype: "json"
            }
        });
        $("#paziente").val(null).trigger("change");



        $("#esami").select2({
            placeholder: 'Cerca Esame',
            width: 'resolve',
            allowClear: true,
            ajax: {
                url: "services/cercaEsami",
                datatype: "json"
            }
        });
        $("#esami").val(null).trigger("change");

        $(document).on("click", "#emettiEsame", function () {



            if (!$('#paziente').val() || !$('#esami').val() || !$('#esito').val()) {
                alert("Inserisci Paziente, Esame ed Esito");
            } else {

                var e = document.getElementById("paziente");
                var strPaziente = e.options[e.selectedIndex].text;
                var codFisc = strPaziente.substr(strPaziente.length - 16);

                var f = document.getElementById("esami");
                var strEsame = f.options[f.selectedIndex].text;

                var esito = $('#esito').val();
                $.post("mediciSpecialisti.handler", {"codiceFiscalePaziente": codFisc, "nomeEsame": strEsame, "esito": esito, "idMedico": <%=authenticatedUser.getId()%>}, function (responseText) {

                    $("#visitaErogata").modal("show");
                    e.remove(e.selectedIndex);
                    f.remove(f.selectedIndex);
                    document.getElementById("esito").value = "";
                });
            }


        });

        var oTblReport = $("#pazientiTable");
        //var tablePazienti;
        var tablePazienti = oTblReport.DataTable({
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "ajax": "services/users",
            "columns": [

                {"data": "nome"},
                {"data": "cognome"},
                {"data": "codiceFiscale"},
                {"data": "dataDiNascita"},
                {"defaultContent": '<center><div class="btn-group"><button id="btnDettagliPaziente" class="btn btn-primary btn-sm" data-toggle="modal" href="#dettagliPaziente" ><img src="images/piuInformazioni.png" height="20" width="20"></button></div></center>'}
            ]
        });


        var tablevisite;
        $(document).on("click", "#btnDettagliPaziente", function () {

            var dati = (tablePazienti.row($(this).parents('tr')).data());
            var string = dati.nome.charAt(0).toUpperCase() + dati.nome.slice(1) + " " + dati.cognome.charAt(0).toUpperCase() + dati.cognome.slice(1);
            $("#nomeCognome").text(string);

            $.get("getDatiPaziente.handler", {"id": dati.id}, function (responseText) {
                //alert(typeof responseText.listaEsami === 'object');
                //alert(responseText.listaEsami);
                $("#imgPaz").attr("src","images/avatars/"+responseText.paziente[0].foto);
                $("#nomePaz").text(responseText.paziente[0].nome);
                $("#cognomePaz").text(responseText.paziente[0].cognome);
                $("#dataDiNascitaPaz").text(responseText.paziente[0].dataDiNascita);
                $("#codiceFiscPaz").text(responseText.paziente[0].codiceFiscale);
                $("#sessoPaz").text(responseText.paziente[0].sesso);
                $("#luogoDiNascitaPaz").text(responseText.paziente[0].luogoDiNascita);
                $("#residenzaPaz").text(responseText.paziente[0].provincia);
                if (typeof responseText.listaEsami === 'object') {
                    //alert("in Esami");


                    var oTblReport = $("#tblReportResultsDemographics");

                    tablePrescrizioniFarmaci = oTblReport.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaFarmaci,
                        "columns": [
                            {"data": "farmaco"},
                            {"data": "descrizione"},
                            {"data": "dataPrescrizione"},
                            {"data": "nomeMedico"},
                            {"data": "cognomeMedico"}

                        ]
                    });
                }

                if (typeof responseText.listaFarmaci === 'object') {
                    //alert("in Farmaci");
                    var oTblReport2 = $("#prescrizioniEsami")

                    tablePrescrizioniEsami = oTblReport2.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaEsami,
                        "columns": [
                            {"data": "esame"},
                            {"data": "descrizione"},
                            {"data": "dataPrescrizione"},
                            {"data": "nomeMedico"},
                            {"data": "cognomeMedico"}

                        ]
                    });
                }

                if (typeof responseText.listaVisiteMedicoDiBase === 'object') {
                    //alert("in Esami");


                    var oTblReport3 = $("#visiteMedicoDiBase");

                    tableVisiteMedicoDiBase = oTblReport3.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaVisiteMedicoDiBase,
                        "columns": [
                            {"data": "nomeMedico"},
                            {"data": "cognomeMedico"},
                            {"data": "descrizione"},
                            {"data": "dataVisita"}

                        ]
                    });
                }

                if (typeof responseText.listaVisite === 'object') {
                    //alert("in Esami");


                    var oTblReport4 = $("#visiteEsami");

                    tablevisite = oTblReport4.DataTable({
                        destroy: true,
                        paging: false,
                        searching: false,
                        "bInfo": false,

                        data: responseText.listaVisite,
                        "columns": [
                            {"data": "nomeMedico"},
                            {"data": "nomeEsame"},
                            {"data": "esito"},
                            {"data": "data"}

                        ]
                    });
                }
            });
        });

    </script>
</html>


