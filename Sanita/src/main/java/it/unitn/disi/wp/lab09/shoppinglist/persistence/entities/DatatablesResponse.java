/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * The entity that describes a Datatables response entity.
 * 
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.05.19
 */
public class DatatablesResponse {
    private Integer draw;
    private Long recordsTotal;
    private Long recordsFiltered;
    private List<Paziente> data;

    public DatatablesResponse(Integer draw, Long recordsTotal, Long recordsFiltered, List<Paziente> data) {
        this.draw = draw;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
        if (data == null) {
            data = new ArrayList<>();
        }
        this.data = data;
    }

    public DatatablesResponse() {
        this(1, 0L, 0L, null);
    }

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public Long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<Paziente> getData() {
        return data;
    }

    public void setData(List<Paziente> data) {
        this.data = data;
    }   
}
