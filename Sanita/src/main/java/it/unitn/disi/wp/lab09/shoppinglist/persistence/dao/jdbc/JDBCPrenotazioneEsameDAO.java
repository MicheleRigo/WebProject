/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;


import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;

import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrenotazioneEsameDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.PrenotazioneEsame;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;



/**
 *
 * @author miche
 */

public class JDBCPrenotazioneEsameDAO extends JDBCDAO<PrenotazioneEsame, Integer> implements PrenotazioneEsameDAO{

    public JDBCPrenotazioneEsameDAO(Connection con) {
        super(con);
        
    }
    
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PrenotazioneEsame getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PrenotazioneEsame> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void creaPrenotazioneEsame(int idMedico, int idSSP, int idPaziente, String dataEsame, String oraEsame, double costo, boolean pagato, int idEsame) throws DAOException {
        
        try {
            if(idMedico!=0){
                //Il medico è uno specialista
                
                PreparedStatement std1 = CON.prepareStatement("INSERT INTO prenotazione_esame (medico,ssp,esame,paziente,data_esame, ora_esame,costo,pagato) values("+idMedico+",null,"+idEsame+","+idPaziente+",'"+dataEsame+"','"+oraEsame+"',"+costo+","+pagato+") ");
                std1.executeUpdate();
            }else{
                //Il medico è un ssp
                System.err.println("idEsame: "+idEsame);
                PreparedStatement std1 = CON.prepareStatement("INSERT INTO prenotazione_esame (medico,ssp,esame,paziente,data_esame, ora_esame,costo,pagato) values(null,"+idSSP+","+idEsame+","+idPaziente+",'"+dataEsame+"','"+oraEsame+"',"+costo+","+pagato+") ");
                std1.executeUpdate();
            }
            


        } catch (Exception e) {
            System.err.println("Errore Insert Prenotazione esame "+e);
        }
    }
    
    @Override
    public LinkedList<PrenotazioneEsame> getPrenotazioni(int idPaziente) throws DAOException{
        LinkedList<PrenotazioneEsame> prenotazioni= new LinkedList<>();
        String query="select s.id as id_medico, s.nome as nome_medico, s.cognome as cognome_medico, e.id as id_esame, e.nome as nome_esame, p.paziente as id_paziente, p.data_esame as data_esame, p.ora_esame as ora_esame, p.costo as costo, p.pagato as pagato from prenotazione_esame p join medico_specialista s on p.medico=s.id join esame e on p.esame=e.id  where SSP is null and p.paziente="+idPaziente;
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {

                while (rs.next()) {
                 
                    PrenotazioneEsame user = new PrenotazioneEsame();
                    user.setIdMedico(rs.getInt("id_medico"));
                    user.setNomeMedico(rs.getString("nome_medico")+" "+rs.getString("cognome_medico"));
                    user.setCognomeMedico(rs.getString("cognome_medico"));
                    user.setIdEsame(rs.getInt("id_esame"));
                    user.setNomeEsame(rs.getString("nome_esame"));
                    user.setIdPaziente(rs.getInt("id_paziente"));
                    user.setDataEsame(rs.getString("data_esame"));
                    user.setOraEsame(rs.getString("ora_esame"));
                    user.setCosto(rs.getDouble("costo"));
                    user.setPagato(rs.getBoolean("pagato"));
                    
                        

                    prenotazioni.add(user);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users (Specialista)", ex);
        }
        
        String query2="select s.id as id_ssp, s.provincia as nome_ssp, e.id as id_esame, e.nome as nome_esame, p.paziente as id_paziente, p.data_esame as data_esame, p.ora_esame as ora_esame, p.costo as costo, p.pagato as pagato from prenotazione_esame p join servizio_sanitario s on p.ssp=s.id join esame e on p.esame=e.id where medico is null and p.paziente="+idPaziente;
        try (Statement stm1 = CON.createStatement()) {
            try (ResultSet rs1 = stm1.executeQuery(query2)) {

                while (rs1.next()) {
                    
                    PrenotazioneEsame user = new PrenotazioneEsame();
                    user.setIdSSP(rs1.getInt("id_ssp"));
                    user.setNomeMedico("SSP "+rs1.getString("nome_ssp"));
                    user.setNomeSSP(rs1.getString("nome_ssp"));
                    user.setIdEsame(rs1.getInt("id_esame"));
                    user.setNomeEsame(rs1.getString("nome_esame"));
                    user.setIdPaziente(rs1.getInt("id_paziente"));
                    user.setDataEsame(rs1.getString("data_esame"));
                    user.setOraEsame(rs1.getString("ora_esame"));
                    user.setCosto(rs1.getDouble("costo"));
                    user.setPagato(rs1.getBoolean("pagato"));
                    
                        

                    prenotazioni.add(user);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users (SSP)", ex);
        }
        
        return prenotazioni;
    }
}
