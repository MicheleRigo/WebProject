/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Farmaco;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author matteo
 */
public interface FarmaciDAO extends DAO<Farmaco,Integer> {
    public LinkedList<Farmaco> getAllFarmaci() throws DAOException;
    
    public List getFarmacoAutocompletamento(String string) throws DAOException;
    
    public int getIdFarmacoByName(String nome) throws DAOException;
}
