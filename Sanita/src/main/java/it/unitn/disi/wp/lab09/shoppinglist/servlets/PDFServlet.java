/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 09 - Shopping List Implementation
 * UniTN
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.*;
import com.google.zxing.common.BitMatrix;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrenotazioneEsameDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrescrizioniDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.DatiRicettaFarmaci;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.PrenotazioneEsame;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;




/**
 * Servlet that handles exporting to PDF.
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.16
 */
public class PDFServlet extends HttpServlet {

    private PrescrizioniDAO prescrizioniDAO;
    private PrenotazioneEsameDAO prenotazioneEsameDAO;

    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            prescrizioniDAO = daoFactory.getDAO(PrescrizioniDAO.class);
            prenotazioneEsameDAO = daoFactory.getDAO(PrenotazioneEsameDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pdfFolder = getServletContext().getInitParameter("pdfFolder");
        if (pdfFolder == null) {
            System.err.println("non trovata pdfFolder");
        }

        pdfFolder = getServletContext().getRealPath(pdfFolder);

        String id = request.getParameter("idUtente");
        

        JSONObject json = new JSONObject();
        
        try {
            LinkedList<PrenotazioneEsame> prenotazioni= prenotazioneEsameDAO.getPrenotazioni(Integer.parseInt(id));
            //crea pdf
            String nomePdf = "ReportTiket" + id + ".pdf";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(pdfFolder + "/" + nomePdf));
            document.open();
            
            document.add(new Paragraph(" "));
            
            Font fontBold = FontFactory.getFont(FontFactory.TIMES_ROMAN, 18, Font.BOLD);
            Font fontHeader = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            
            Chunk chunk = new Chunk("Report Ticket", fontBold);
            document.add(chunk);
            document.add(new Paragraph(" "));
            
           
            PdfPTable table = new PdfPTable(3);
            
            String[] headers= new String[]{"Esame","Costo","Data"};
             for (String header : headers) {
                PdfPCell cell = new PdfPCell();
                cell.setGrayFill(0.9f);
                cell.setPhrase(new Phrase(header.toUpperCase(), fontHeader));
                table.addCell(cell);
            }
            table.completeRow();
            
            for(PrenotazioneEsame p:prenotazioni){
                
                PdfPCell cell = new PdfPCell();
                cell.setPhrase(new Phrase(p.getNomeEsame(), fontHeader));
                table.addCell(cell);
                
                
                cell = new PdfPCell();
                cell.setPhrase(new Phrase(p.getCosto()+"", fontHeader));
                table.addCell(cell);
                
                
                cell = new PdfPCell();
                cell.setPhrase(new Phrase(p.getDataEsame(), fontHeader));
                table.addCell(cell);
                table.completeRow();
                
            }
            
            document.addTitle("PDF Report Ticket");
                
            document.add(table);
                
            document.close();
            json = new JSONObject();
            json.putOpt("path", nomePdf);

        } catch (DocumentException ex) {
            System.err.println("" + ex);
        } catch (DAOException ex) {
            System.err.println(ex);
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

    



@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pdfFolder = getServletContext().getInitParameter("pdfFolder");
        if (pdfFolder == null) {
            System.err.println("non trovata pdfFolder");
        }

        pdfFolder = getServletContext().getRealPath(pdfFolder);

        String id = request.getParameter("id");
        System.err.println(id);
        if (id == null) {
            id = "0";
        }

        JSONObject json = new JSONObject();
        try {
            DatiRicettaFarmaci d = prescrizioniDAO.getDatiRicetta(Integer.parseInt(id));
            //System.err.println(d.toString());
            //Crea qr
            String nomeQR = "QR" + id + ".png";
            generateQRCodeImage(d.toString(), pdfFolder + "/" + nomeQR);
            //crea pdf
            String nomePdf = "PDF" + id + ".pdf";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(pdfFolder + "/" + nomePdf));
            document.open();

            //Scrive sul pdf
            Font fontBold = FontFactory.getFont(FontFactory.COURIER, 16, Font.BOLD);
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, Font.ITALIC);
            Chunk chunk = new Chunk("Ricetta", fontBold);
            document.add(chunk);
            //document.add(Chunk.NEWLINE);
            document.add(new Paragraph(""));

            Image img = null;
            //Aggiunge qr
            img = Image.getInstance(pdfFolder + "/" + nomeQR);
            document.add(img);

            Chunk chunk1 = new Chunk("Paziente: ", fontBold);
            Chunk c1 = new Chunk("second", font);
            /*document.add(chunk);
            document.add(c1);*/

            document.add(new Paragraph(""));

            document.close();

            json = new JSONObject();
            json.putOpt("path", nomePdf);

        } catch (WriterException ex) {
            System.err.println("WriterException Error" + ex);

} catch (DocumentException ex) {
            Logger.getLogger(PDFServlet.class  

.getName()).log(Level.SEVERE, null, ex);
        }

catch (DAOException ex) {
            Logger.getLogger(PDFServlet.class  

.getName()).log(Level.SEVERE, null, ex);
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());

    }

    private static void generateQRCodeImage(String text, String filePath)
            throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, 350, 350);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
    }
}
