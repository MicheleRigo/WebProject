/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author miche
 */
public class EsamiDiOggi {
    
    private String nomePaziente;
    private String cognomePaziente;
    private String dataDiNascitaPaziente;
    private String nomeEsame;
    private String dataEsame;
    private String oraEsame;

    public String getNomePaziente() {
        return nomePaziente;
    }

    public void setNomePaziente(String nomePaziente) {
        this.nomePaziente = nomePaziente;
    }

    public String getCognomePaziente() {
        return cognomePaziente;
    }

    public void setCognomePaziente(String cognomePaziente) {
        this.cognomePaziente = cognomePaziente;
    }

    public String getDataDiNascitaPaziente() {
        return dataDiNascitaPaziente;
    }

    public void setDataDiNascitaPaziente(String dataDiNascitaPaziente) {
        this.dataDiNascitaPaziente = dataDiNascitaPaziente;
    }

    public String getNomeEsame() {
        return nomeEsame;
    }

    public void setNomeEsame(String nomeEsame) {
        this.nomeEsame = nomeEsame;
    }

    public String getDataEsame() {
        return dataEsame;
    }

    public void setDataEsame(String dataEsame) {
        this.dataEsame = dataEsame;
    }

    public String getOraEsame() {
        return oraEsame;
    }

    public void setOraEsame(String oraEsame) {
        this.oraEsame = oraEsame;
    }
    
    
    
}
