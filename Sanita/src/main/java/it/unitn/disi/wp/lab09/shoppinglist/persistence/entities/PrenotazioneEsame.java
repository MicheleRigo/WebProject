/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author miche
 */
public class PrenotazioneEsame {
    private int idMedico;
    private String nomeMedico;
    private String cognomeMedico;
    
    private String nomeSSP;
    private int idSSP;

    private int idEsame;
    private String nomeEsame;
    
    private int idPaziente;
    private String dataEsame;
    private String oraEsame;
    private double costo;
    private boolean pagato;
    
    public int getIdSSP() {
        return idSSP;
    }

    public void setIdSSP(int idSSP) {
        this.idSSP = idSSP;
    }

    public int getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }

    public String getNomeMedico() {
        return nomeMedico;
    }

    public void setNomeMedico(String nomeMedico) {
        this.nomeMedico = nomeMedico;
    }

    public String getCognomeMedico() {
        return cognomeMedico;
    }

    public void setCognomeMedico(String cognomeMedico) {
        this.cognomeMedico = cognomeMedico;
    }

    public String getNomeSSP() {
        return nomeSSP;
    }

    public void setNomeSSP(String nomeSSP) {
        this.nomeSSP = nomeSSP;
    }

    public int getIdEsame() {
        return idEsame;
    }

    public void setIdEsame(int idEsame) {
        this.idEsame = idEsame;
    }

    public String getNomeEsame() {
        return nomeEsame;
    }

    public void setNomeEsame(String nomeEsame) {
        this.nomeEsame = nomeEsame;
    }

    public int getIdPaziente() {
        return idPaziente;
    }

    public void setIdPaziente(int idPaziente) {
        this.idPaziente = idPaziente;
    }

    public String getDataEsame() {
        return dataEsame;
    }

    public void setDataEsame(String dataEsame) {
        this.dataEsame = dataEsame;
    }

    public String getOraEsame() {
        return oraEsame;
    }

    public void setOraEsame(String oraEsame) {
        this.oraEsame = oraEsame;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public boolean isPagato() {
        return pagato;
    }

    public void setPagato(boolean pagato) {
        this.pagato = pagato;
    }
    
    
    
}
