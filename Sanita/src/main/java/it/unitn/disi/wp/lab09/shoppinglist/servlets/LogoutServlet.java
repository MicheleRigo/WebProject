/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.CookieDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author miche
 */
public class LogoutServlet extends HttpServlet {

    
    /*protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LogoutServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LogoutServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
*/
    private UserDAO userDao;
    private CookieDAO cookieDao;
    private final HashMap<String, Integer> authenticatedUsers;

    public LogoutServlet() {
        super();
        authenticatedUsers = new HashMap<>();
    }
    

    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            cookieDao=daoFactory.getDAO(CookieDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.getSession().removeAttribute("user");
        
        Cookie[] cookies = request.getCookies();
        if (cookies != null){
            for (Cookie cookie : cookies) { 
                    switch (cookie.getName()) {
                        case "login":
                            cookie.setMaxAge(0);
                            System.out.println("Cancellare cookie: "+cookie.getValue());
                            cookieDao.deleteCookie(cookie.getValue());
                            response.addCookie(cookie);
                            break;
                } 
            }
        }
        response.sendRedirect("index.html");
    }

    public void dispatch(javax.servlet.http.HttpServletRequest request,
        javax.servlet.http.HttpServletResponse response, String nextPage)
        throws ServletException, IOException {


        RequestDispatcher dispatch = request.getRequestDispatcher(nextPage);
        dispatch.forward(request, response);

    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
