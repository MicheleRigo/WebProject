/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author miche
 */
public class EsamePaziente {
    private int id;
    private int prenotazione;
    private String esito;
    private int idSpecialista;
    private int idSSP;
    private int idEsame;//id della tabella degli esami che possono essere fatti
    private int idPaziente;
    private String data;
    private String ora;
    
    private String nomeSpecialista;
    private String cognomeSpecialista;
    private String nomeSSP;
    private String nomeEsame;

    public String getNomeSpecialista() {
        return nomeSpecialista;
    }

    public void setNomeSpecialista(String nomeSpecialista) {
        this.nomeSpecialista = nomeSpecialista;
    }

    public String getCognomeSpecialista() {
        return cognomeSpecialista;
    }

    public void setCognomeSpecialista(String cognomeSpecialista) {
        this.cognomeSpecialista = cognomeSpecialista;
    }

    public String getNomeSSP() {
        return nomeSSP;
    }

    public void setNomeSSP(String nomeSSP) {
        this.nomeSSP = nomeSSP;
    }

    public String getNomeEsame() {
        return nomeEsame;
    }

    public void setNomeEsame(String nomeEsame) {
        this.nomeEsame = nomeEsame;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrenotazione() {
        return prenotazione;
    }

    public void setPrenotazione(int prenotazione) {
        this.prenotazione = prenotazione;
    }

    public String getEsito() {
        return esito;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

    public int getIdSpecialista() {
        return idSpecialista;
    }

    public void setIdSpecialista(int idSpecialista) {
        this.idSpecialista = idSpecialista;
    }

    public int getIdSSP() {
        return idSSP;
    }

    public void setIdSSP(int idSSP) {
        this.idSSP = idSSP;
    }

    public int getIdEsame() {
        return idEsame;
    }

    public void setIdEsame(int idEsame) {
        this.idEsame = idEsame;
    }

    public int getIdPaziente() {
        return idPaziente;
    }

    public void setIdPaziente(int idPaziente) {
        this.idPaziente = idPaziente;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOra() {
        return ora;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }
    
    
    
}
