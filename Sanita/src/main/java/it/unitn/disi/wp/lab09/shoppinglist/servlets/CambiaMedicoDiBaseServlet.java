/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.CambiaMedicoDiBaseDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.ParcoPazientiDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author miche
 */
public class CambiaMedicoDiBaseServlet extends HttpServlet {

    private UserDAO userDao;
    private ParcoPazientiDAO parcoPazientiDAO;
    private CambiaMedicoDiBaseDAO cambiaMedicoDiBaseDAO; 

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            parcoPazientiDAO= daoFactory.getDAO(ParcoPazientiDAO.class);
            cambiaMedicoDiBaseDAO= daoFactory.getDAO(CambiaMedicoDiBaseDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id= Integer.parseInt(request.getParameter("id"));
        String provincia= request.getParameter("provincia");
        
        
        JSONObject json= new JSONObject();
        
        try {
            
            
            
            json= new JSONObject();
                    
                    json.put("listaMedici", new JSONArray(cambiaMedicoDiBaseDAO.getAllMedicoDiBase(provincia)));
                    json.put("medicoCorrente", new JSONObject(parcoPazientiDAO.getMedicoDiBaseByPaziente(id)));
           
                    
            
        } catch (DAOException ex) {
            Logger.getLogger(GetDatiPazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("Non Sono riuscito a costruire il json");
        }
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nome = request.getParameter("nomeMed");
        String cognome = request.getParameter("cognomeMed");
        String via= request.getParameter("viaMed");
        String civico= request.getParameter("civicoMed");
        String cap= request.getParameter("capMed");
        String provincia= request.getParameter("provinciaMed");

        int idPaziente= Integer.parseInt(request.getParameter("idUtente"));
        try {
            int idMedicoVecchio=((MedicoDiBase)parcoPazientiDAO.getMedicoDiBaseByPaziente(idPaziente)).getId();
            int idMedicoNuovo= userDao.getIdMedicoDiBase(nome, cognome, via, civico, cap, provincia);
            
            if(cambiaMedicoDiBaseDAO.cambiaMedicoDiBase(idPaziente, idMedicoNuovo, idMedicoVecchio)){
                
                ((Paziente)request.getSession().getAttribute("user")).setMedicoDiBase(parcoPazientiDAO.getMedicoDiBaseByPaziente(idPaziente));
                
                dispatch(request, response, "users.jsp");
            }

        } catch (DAOException ex) {
            System.err.println("Problemi a trovare il medico");
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public void dispatch(javax.servlet.http.HttpServletRequest request,
        javax.servlet.http.HttpServletResponse response, String nextPage)
        throws ServletException, IOException {

        RequestDispatcher dispatch = request.getRequestDispatcher(nextPage);
        dispatch.forward(request, response);

}

}
