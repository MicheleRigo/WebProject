/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author miche
 */
public class DatiRicettaFarmaci {
    
    private int codiceMedico;
    private String codiceFiscalePaziente;
    private String dataPrescrizione;
    private int codicePrescrizione;
    private String descrizioneFarmaco;

    public int getCodiceMedico() {
        return codiceMedico;
    }

    public void setCodiceMedico(int codiceMedico) {
        this.codiceMedico = codiceMedico;
    }

    public String getCodiceFiscalePaziente() {
        return codiceFiscalePaziente;
    }

    public void setCodiceFiscalePaziente(String codiceFiscalePaziente) {
        this.codiceFiscalePaziente = codiceFiscalePaziente;
    }

    public String getDataPrescrizione() {
        return dataPrescrizione;
    }

    public void setDataPrescrizione(String dataPrescrizione) {
        this.dataPrescrizione = dataPrescrizione;
    }

    public int getCodicePrescrizione() {
        return codicePrescrizione;
    }

    public void setCodicePrescrizione(int codicePrescrizione) {
        this.codicePrescrizione = codicePrescrizione;
    }

    public String getDescrizioneFarmaco() {
        return descrizioneFarmaco;
    }

    public void setDescrizioneFarmaco(String descrizioneFarmaco) {
        this.descrizioneFarmaco = descrizioneFarmaco;
    }
    
    @Override
    public String toString(){
        String res="Codice Medico: "+this.getCodiceMedico()+"\n"+
                
                "Codice Fiscale Paziente: "+this.getCodiceFiscalePaziente()+"\n"+
                "Data prescrizione: "+this.getDataPrescrizione()+"\n"+
                "Codice prescrizione: "+this.getCodicePrescrizione()+"\n"+
                "Descrizione farmaco: "+this.getDescrizioneFarmaco();
        return res;
    }
    /*private int codiceMedico;
    private String codiceFiscalePaziente;
    private String dataPrescrizione;
    private int codicePrescrizione;
    private String descrizioneFarmaco;*/
    
}
