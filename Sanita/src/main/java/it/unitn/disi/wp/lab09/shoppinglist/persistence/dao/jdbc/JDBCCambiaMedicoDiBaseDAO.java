/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.CambiaMedicoDiBaseDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author miche
 */
public class JDBCCambiaMedicoDiBaseDAO extends JDBCDAO<MedicoDiBase, Integer> implements CambiaMedicoDiBaseDAO{

   
    
    public JDBCCambiaMedicoDiBaseDAO(Connection con) {
        super(con);
        
    }
    
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MedicoDiBase getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<MedicoDiBase> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<MedicoDiBase> getAllMedicoDiBase(String provincia) throws DAOException {
        
        LinkedList<MedicoDiBase> medici= new LinkedList<MedicoDiBase>();
        String query="SELECT * FROM MEDICO_DI_BASE WHERE PROVINCIA='"+provincia+"'";
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {

                while (rs.next()) {
                    MedicoDiBase user = new MedicoDiBase();
                    
                        user.setId(rs.getInt("id"));
                        user.setNome(rs.getString("nome"));
                        user.setCognome(rs.getString("cognome"));
                        user.setCapStudio(rs.getString("cap"));
                        user.setNumeroCivicoStudio(rs.getString("civico"));
                        user.setViaStudio(rs.getString("via"));
                        user.setSesso(rs.getString("sesso"));
                        user.setProvincia(rs.getString("provincia"));

                    medici.add(user);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        
        
          
        
        return medici;
    }

    
    
    @Override
    public boolean cambiaMedicoDiBase(int idPaziente, int idMedicoNuovo, int idMedicoVecchio)throws DAOException{
    
        boolean res= false;
        
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String dataFine=formatter.format(date).toString();
        try{
            PreparedStatement std = CON.prepareStatement("UPDATE PARCO_PAZIENTI SET DATA_FINE= '"+dataFine+"' WHERE ID_MEDICO="+idMedicoVecchio+" AND ID_PAZIENTE="+idPaziente);
            std.executeUpdate();
            
            PreparedStatement std2 = CON.prepareStatement("UPDATE PARCO_PAZIENTI SET ATTIVO = FALSE WHERE ID_MEDICO="+idMedicoVecchio+" AND ID_PAZIENTE="+idPaziente);
            std2.executeUpdate();
            try {
                PreparedStatement std1 = CON.prepareStatement("INSERT INTO PARCO_PAZIENTI (ID_MEDICO,ID_PAZIENTE, DATA_INIZIO, DATA_FINE, ATTIVO) VALUES ("+idMedicoNuovo+","+idPaziente+", '"+dataFine+"' , null, true)");
                std1.executeUpdate();
                
                
            } catch (Exception e) {
                System.err.println("Errore Insert parco pazienti "+e);
            }
            
        }catch(Exception e){
            System.err.println("Errore Update parco pazienti "+ e);
        }
        
        
            
            
            res=true;


        
            
        
        return res;        
    }
   
    
    
    
}
