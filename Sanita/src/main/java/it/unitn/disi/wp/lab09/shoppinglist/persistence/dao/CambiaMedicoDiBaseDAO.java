/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import java.util.LinkedList;

/**
 *
 * @author miche
 */
public interface CambiaMedicoDiBaseDAO extends DAO<MedicoDiBase,Integer>{
    
    public LinkedList<MedicoDiBase> getAllMedicoDiBase(String provincia) throws DAOException;
    
    public boolean cambiaMedicoDiBase(int idPaziente, int idMedicoNuovo, int idMedicoVecchio) throws DAOException;
}
