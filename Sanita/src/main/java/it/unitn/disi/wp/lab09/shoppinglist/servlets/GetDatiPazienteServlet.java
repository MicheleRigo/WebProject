/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrescrizioniDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.VisiteMedicoDiBasePazienteDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoSpecialista;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.EsamePazienteDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrenotazioneEsameDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.VisitePazienteDAO;
import java.util.LinkedList;

/**
 *
 * @author miche
 */
public class GetDatiPazienteServlet extends HttpServlet {

    private UserDAO userDao;
    private PrescrizioniDAO prescrizioniDAO;
    private VisiteMedicoDiBasePazienteDAO visiteMedicoDiBasePazienteDAO;
    private VisitePazienteDAO visitePazienteDAO;
    private EsamePazienteDAO esamePazienteDAO;
    private PrenotazioneEsameDAO prenotazioneEsameDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            prescrizioniDAO = daoFactory.getDAO(PrescrizioniDAO.class);
            visiteMedicoDiBasePazienteDAO = daoFactory.getDAO(VisiteMedicoDiBasePazienteDAO.class);
            visitePazienteDAO= daoFactory.getDAO(VisitePazienteDAO.class);
            esamePazienteDAO= daoFactory.getDAO(EsamePazienteDAO.class);
            prenotazioneEsameDAO = daoFactory.getDAO(PrenotazioneEsameDAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id= Integer.parseInt(request.getParameter("id"));
        
        JSONObject json= new JSONObject();
        
        
        try {
            
            
            
            json= new JSONObject();
                    json.putOpt("listaEsami", new JSONArray(prescrizioniDAO.getAllPrescrizioniEsami(id)));
                    json.putOpt("listaFarmaci", new JSONArray(prescrizioniDAO.getAllPrescrizioniFarmaci(id)));
                    json.putOpt("listaVisiteMedicoDiBase", new JSONArray(visiteMedicoDiBasePazienteDAO.getVisiteMedicoDiBasePaziente(id)));
                    json.putOpt("listaVisite", new JSONArray(visitePazienteDAO.getVisitePaziente(id)));
                    
                    json.putOpt("listaPrenotazioni", new JSONArray(prenotazioneEsameDAO.getPrenotazioni(id)));
                    LinkedList a = new LinkedList();
                    a.add(userDao.getPazienteById(id));
                    json.putOpt("paziente", new JSONArray(a));
           
                    
           
        } catch (DAOException ex) {
            Logger.getLogger(GetDatiPazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }
    
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void dispatch(javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response, String nextPage)
            throws ServletException, IOException {

        RequestDispatcher dispatch = request.getRequestDispatcher(nextPage);
        dispatch.forward(request, response);

    }

}
