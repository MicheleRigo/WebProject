/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ServizioSanitarioProvinciale;
import java.util.List;

/**
 *
 * @author miche
 */
public interface SspDAO extends DAO<ServizioSanitarioProvinciale,Integer>{
    
    public List getEsamiOggi(int idSSP) throws DAOException;
    
    public List getRicette(String provincia) throws DAOException;
    
}
