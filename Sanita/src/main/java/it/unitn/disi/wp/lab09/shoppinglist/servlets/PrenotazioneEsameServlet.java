/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrenotazioneEsameDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrescrizioniDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author miche
 */
@WebServlet(name = "PrenotazioneEsameServlet", urlPatterns = {"/prenotazioneEsame.handler"})
public class PrenotazioneEsameServlet extends HttpServlet {

    private UserDAO userDao;
    private PrenotazioneEsameDAO prenotazioneEsameDao;
    private PrescrizioniDAO prescrizioniDAO;
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            prenotazioneEsameDao = daoFactory.getDAO(PrenotazioneEsameDAO.class);
            prescrizioniDAO= daoFactory.getDAO(PrescrizioniDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            int idEsame = Integer.parseInt(request.getParameter("idEsame"));
            int idPaziente = Integer.parseInt(request.getParameter("idPaziente"));
            String dataEsame = request.getParameter("dataEsame");
            String oraEsame = request.getParameter("oraEsame");
            int idMedico = Integer.parseInt(request.getParameter("idMedico"));
            int idSSP;//da trovare in base alla provincia se idMedico==0
            String provincia = request.getParameter("provincia");
            double costo = Double.parseDouble(request.getParameter("costo"));
            
            int idPrescrizione= Integer.parseInt(request.getParameter("idPrescrizione"));
            
            boolean pagato = Boolean.getBoolean(request.getParameter("pagato"));
            pagato=true;
           
            
            
            //System.err.println(idPaziente+" "+idEsame+" "+dataEsame+" "+oraEsame+" "+idMedico+" "+provincia+" "+costo+" "+pagato);
            
            if(idMedico==0){
                try {
                    idSSP=userDao.getIdServizioSanitario(provincia);
                    System.err.println(idSSP);
                    if(idSSP!=0){
                        prenotazioneEsameDao.creaPrenotazioneEsame(0, idSSP, idPaziente, dataEsame, oraEsame, costo, pagato, idEsame);
                        prescrizioniDAO.setPrenotato(idPrescrizione);
                    }
                } catch (DAOException ex) {
                    System.err.println(ex);
                }
            }else{
                try {
                    prenotazioneEsameDao.creaPrenotazioneEsame(idMedico, 0, idPaziente, dataEsame, oraEsame, costo, pagato, idEsame);
                    prescrizioniDAO.setPrenotato(idPrescrizione);
                } catch (DAOException ex) {
                    System.err.println(ex);
                }
            
            }
       
            
            
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void dispatch(javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response, String nextPage)
            throws ServletException, IOException {

        RequestDispatcher dispatch = request.getRequestDispatcher(nextPage);
        dispatch.forward(request, response);

    }


}
