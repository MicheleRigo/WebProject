/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.SspDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.EsamiDiOggi;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ReportPrescrizioneFarmaci;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ServizioSanitarioProvinciale;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author miche
 */
public class JDBCSspDAO extends JDBCDAO<ServizioSanitarioProvinciale, Integer> implements SspDAO{

    public JDBCSspDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServizioSanitarioProvinciale getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ServizioSanitarioProvinciale> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List getEsamiOggi(int idMedico) throws DAOException {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String dataVisita=formatter.format(date).toString();
        
        LinkedList<EsamiDiOggi> listaPrenotazioni= new LinkedList<EsamiDiOggi>();
        String query="select paz.nome as nome_paziente, paz.cognome as cognome_paziente, paz.data_di_nascita, e.nome as nome_esame, p.data_esame, p.ora_esame from prenotazione_esame p join paziente paz on p.paziente=paz.id join esame e on p.esame=e.id where ssp="+idMedico+" and data_esame='"+dataVisita+"'";
        
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {
                
                while (rs.next()) {
                    EsamiDiOggi p= new EsamiDiOggi();
                    
                    p.setNomePaziente(rs.getString("nome_paziente"));
                    p.setCognomePaziente(rs.getString("cognome_paziente"));
                    p.setDataDiNascitaPaziente(rs.getString("data_di_nascita"));
                    p.setNomeEsame(rs.getString("nome_esame"));
                    p.setDataEsame(rs.getString("data_esame"));
                    p.setOraEsame(rs.getString("ora_esame"));
                    
                        
                        

                    listaPrenotazioni.add(p);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list Esami di oggi", ex);
        }
        
        
        return listaPrenotazioni;
    }
    
    
    @Override
    public List getRicette(String provincia) throws DAOException {
        LinkedList<ReportPrescrizioneFarmaci> listaPrescrizioniFarmaci= new LinkedList<ReportPrescrizioneFarmaci>();
        String query="select p.data_prescrizione as data, f.nome as nome_farmaco, m.nome as nome_medico, m.cognome as cognome_medico, paz.nome as nome_paziente,paz.cognome as cognome_paziente, f.costo as costo_farmaco from prescrizione p join medico_di_base m on p.medico=m.id join paziente paz on p.paziente=paz.id join farmaco f on p.farmaco=f.id where p.esame is null and paz.provincia='"+provincia+"'";
        
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {
                
                while (rs.next()) {
                    ReportPrescrizioneFarmaci r = new ReportPrescrizioneFarmaci();
                    
                    r.setData(rs.getString("data"));
                    r.setNomeFarmaco(rs.getString("nome_farmaco"));
                    r.setNomeCognomeMedico(rs.getString("nome_medico")+" "+rs.getString("cognome_medico"));
                    r.setNomeCognomePaziente(rs.getString("nome_paziente")+" "+rs.getString("cognome_paziente"));
                    r.setCostoFarmaco(rs.getDouble("costo_farmaco"));
                    
                    listaPrescrizioniFarmaci.add(r);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescrizione farmaci per SSP", ex);
        }
        
        
        return listaPrescrizioniFarmaci;
    }
    
}
