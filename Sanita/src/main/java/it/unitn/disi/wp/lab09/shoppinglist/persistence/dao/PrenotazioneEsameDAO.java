/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.PrenotazioneEsame;
import java.util.LinkedList;

/**
 *
 * @author miche
 */
public interface PrenotazioneEsameDAO extends DAO<PrenotazioneEsame,Integer>{
    
    public void creaPrenotazioneEsame(int idMedico, int idSSP, int idPaziente, String dataEsame, String oraEsame, double costo, boolean pagato, int idEsame) throws  DAOException;
    
    public LinkedList<PrenotazioneEsame> getPrenotazioni(int idPaziente) throws DAOException;
}
