/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.FarmaciDAO;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Farmaco;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ObjectPerAutocompletamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author matteo
 */
public class JDBCFarmaciDAO extends JDBCDAO<Farmaco, Integer> implements FarmaciDAO{

    public JDBCFarmaciDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Farmaco getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Farmaco> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<Farmaco> getAllFarmaci() throws DAOException {
        LinkedList<Farmaco> listaFarmaci= new LinkedList<Farmaco>();
        String query="SELECT * FROM FARMACO";
        
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {

                while (rs.next()) {
                    Farmaco frmc = new Farmaco();
                    
                        frmc.setNome(rs.getString("nome"));
                        frmc.setId(rs.getInt("id"));
                        

                    listaFarmaci.add(frmc);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of farmaci", ex);
        }
        
        return listaFarmaci;
    }
    
    @Override
    public List getFarmacoAutocompletamento(String string) throws DAOException {
        String query = "select nome from farmaco where ucase(nome) LIKE '%"+string+"%'";

        try (PreparedStatement stm = CON.prepareStatement(query)) {
            
            try (ResultSet rs = stm.executeQuery()) {
                

                List<ObjectPerAutocompletamento> farmaci = new ArrayList<ObjectPerAutocompletamento>();
                int i=1;
                while (rs.next()) {
                    ObjectPerAutocompletamento o = new ObjectPerAutocompletamento();
                    o.setId(i);
                    o.setText(rs.getString("nome").toUpperCase());
                    farmaci.add(o);
                    i++;
                }
                return farmaci;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            throw new DAOException("Impossible to get the list of farmaci", ex);
        }
    }

    @Override
    public int getIdFarmacoByName(String nome) throws DAOException {
        String query = "SELECT ID FROM farmaco WHERE ucase(NOME)='" + nome.toUpperCase() + "'";
        int id = 0;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {
                while (rs.next()) {
                    id = Integer.parseInt(rs.getString("id"));
                }

            }
        } catch (SQLException ex) {
            throw new DAOException("Impossibile trovare il farmaco: ", ex);
        }

        return id;
    }
    
}