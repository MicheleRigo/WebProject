/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.05.19
 */
@javax.ws.rs.ApplicationPath("services")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(it.unitn.disi.wp.lab09.shoppinglist.services.CercaPazienteService.class);
        resources.add(it.unitn.disi.wp.lab09.shoppinglist.services.EsamiService.class);
        resources.add(it.unitn.disi.wp.lab09.shoppinglist.services.FarmaciService.class);
        resources.add(it.unitn.disi.wp.lab09.shoppinglist.services.ListaPazientiService.class);
    }
    
}

