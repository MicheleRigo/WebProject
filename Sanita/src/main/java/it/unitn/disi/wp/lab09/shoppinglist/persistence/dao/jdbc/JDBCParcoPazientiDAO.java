/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.ParcoPazientiDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import java.sql.Connection;

/**
 *
 * @author miche
 */
public class JDBCParcoPazientiDAO extends JDBCDAO<Paziente, Integer> implements ParcoPazientiDAO{

    public JDBCParcoPazientiDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Paziente getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Paziente> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<Paziente> getAllPaziente(int idMedico) throws DAOException {
        
        
        LinkedList<Paziente> pazienti= new LinkedList<Paziente>();
        String query="SELECT * FROM PARCO_PAZIENTI PAR JOIN PAZIENTE PAZ ON PAR.ID_PAZIENTE=PAZ.ID WHERE DATA_FINE IS NULL AND PAR.id_medico="+idMedico;
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {

                while (rs.next()) {
                    Paziente user = new Paziente();
                    user.setId(rs.getInt("id_paziente"));
                    user.setNome(rs.getString("nome"));
                    user.setCognome(rs.getString("cognome"));
                    user.setSesso(rs.getString("sesso"));
                    user.setCodiceFiscale(rs.getString("codice_fiscale"));
                    user.setFoto(rs.getString("foto"));
                    user.setDataDiNascita(rs.getString("data_di_nascita"));
                    user.setLuogoDiNascita(rs.getString("luogo_di_nascita"));
                    user.setProvincia(rs.getString("provincia"));
                    
                    

                    pazienti.add(user);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        
        
          
       
        return pazienti;
    }

    @Override
    public MedicoDiBase getMedicoDiBaseByPaziente(int idUtente) throws DAOException {
        String query="SELECT M.ID AS ID, M.NOME AS NOME, M.COGNOME as COGNOME FROM PARCO_PAZIENTI PAR JOIN MEDICO_DI_BASE M ON PAR.ID_MEDICO=M.ID WHERE DATA_FINE IS NULL AND PAR.ID_PAZIENTE = "+idUtente;
        try (Statement stm1 = CON.createStatement()) {
            try (ResultSet rs = stm1.executeQuery(query)) {
                    
                    MedicoDiBase medico= new MedicoDiBase();
                    while (rs.next()) {
                        
                        medico.setId(rs.getInt("id"));
                        medico.setNome(rs.getString("nome"));
                        medico.setCognome(rs.getString("cognome"));
                    }
                 return medico;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            MedicoDiBase med=new MedicoDiBase();
            med.setNome("Null");
            med.setCognome("Null");
            return med;
         }
    }
    
    
    
    
    

    
    
}
