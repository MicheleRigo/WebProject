/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.MedicoSpecialistaDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoSpecialista;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;

import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.EsamiDiOggi;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author miche
 */
public class JDBCMedicoSpecialistaDAO extends JDBCDAO<MedicoSpecialista,Integer> implements MedicoSpecialistaDAO{

    public JDBCMedicoSpecialistaDAO(Connection con) {
        super(con);
        
    }
    
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MedicoSpecialista getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<MedicoSpecialista> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<MedicoSpecialista> getMedicoSpecialistaByProvincia(String provincia, String specializzazione) throws DAOException {
        LinkedList<MedicoSpecialista> listaMedici= new LinkedList<MedicoSpecialista>();
        String query="SELECT * FROM MEDICO_SPECIALISTA WHERE PROVINCIA='"+provincia+"' and specializzazione="+specializzazione+"";
        
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {
                
                while (rs.next()) {
                    MedicoSpecialista medico = new MedicoSpecialista();
                     medico.setNome(rs.getString("nome"));
                     medico.setCognome(rs.getString("cognome"));
                     medico.setViaStudio(rs.getString("via"));
                     medico.setNumeroCivicoStudio(rs.getString("civico"));
                     medico.setId(rs.getInt("id"));
                     
                        
                        

                    listaMedici.add(medico);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of medici specialisti", ex);
        }
        
        return listaMedici;
    }

    @Override
    public List getEsamiOggi(int idMedico) throws DAOException {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String dataVisita=formatter.format(date).toString();
        
        LinkedList<EsamiDiOggi> listaPrenotazioni= new LinkedList<EsamiDiOggi>();
        String query="select paz.nome as nome_paziente, paz.cognome as cognome_paziente, paz.data_di_nascita, e.nome as nome_esame, p.data_esame, p.ora_esame from prenotazione_esame p join paziente paz on p.paziente=paz.id join esame e on p.esame=e.id where medico="+idMedico+" and data_esame='"+dataVisita+"'";
        
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {
                
                while (rs.next()) {
                    EsamiDiOggi p= new EsamiDiOggi();
                    
                    p.setNomePaziente(rs.getString("nome_paziente"));
                    p.setCognomePaziente(rs.getString("cognome_paziente"));
                    p.setDataDiNascitaPaziente(rs.getString("data_di_nascita"));
                    p.setNomeEsame(rs.getString("nome_esame"));
                    p.setDataEsame(rs.getString("data_esame"));
                    p.setOraEsame(rs.getString("ora_esame"));
                    
                        
                        

                    listaPrenotazioni.add(p);
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new DAOException("Impossible to get the list Esami di oggi", ex);
        }
        
        
        return listaPrenotazioni;
    }
    
}
