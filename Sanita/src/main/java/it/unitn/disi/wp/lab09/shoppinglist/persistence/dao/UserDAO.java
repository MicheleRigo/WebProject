/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 09 - Shopping List Implementation
 * UniTN
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente;
import java.util.List;

/**
 * All concrete DAOs must implement this interface to handle the persistence 
 * system that interact with {@link User users}.
 * 
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public interface UserDAO extends DAO<Utente, Integer> {
    /**
     * Returns the {@link User user} with the given {@code email} and
     * {@code password}.
     * @param email the email of the user to get.
     * @param password the password of the user to get.
     * @return the {@link User user} with the given {@code username} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     * 
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    public Utente getByEmailAndPassword(String email, String password) throws DAOException;
    
    /**
     * Update the user passed as parameter and returns it.
     * @param user the user used to update the persistence system.
     * @return the updated user.
     * @throws DAOException if an error occurred during the action.
     * 
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    public Utente update(Utente user) throws DAOException;
    
    public Utente getByEmail(String email) throws DAOException;
    
    public boolean setPassword(int idUtente, String password) throws DAOException;
    
    public int getIdMedicoDiBase(String nome, String cognome, String via, String civico, String cap, String provincia)throws DAOException;
    
    public int getByCodiceFiscale(String codiceFiscale) throws DAOException;
    
    public int getIdServizioSanitario(String provincia) throws DAOException;
    
    public MedicoDiBase getMedicoDiBaseByPaziente(int idUtente) throws DAOException;
    
    public Long getCountPaziente() throws DAOException;
    
    public List pageBySearchValue(String searchValue, Long start, Long length) throws DAOException;
    
    public List getPazienteAutocomplete(String string) throws DAOException;
    
    public Paziente getPazienteById(int id) throws DAOException;
    
    public void setFotoPaziente(int id, String path) throws DAOException;
    
    public String getPazienteMailById(int id) throws DAOException;
    
}
