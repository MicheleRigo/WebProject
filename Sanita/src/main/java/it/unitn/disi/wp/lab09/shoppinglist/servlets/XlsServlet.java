/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.SspDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ReportPrescrizioneFarmaci;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;
/**
 *
 * @author miche
 */
public class XlsServlet extends HttpServlet {

    private UserDAO userDao;
    private SspDAO sspDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            sspDAO = daoFactory.getDAO(SspDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String xlsFolder = getServletContext().getInitParameter("xlsFolder");
        if (xlsFolder == null) {
            System.err.println("non trovata xlsFolder");
        }
        
        xlsFolder=getServletContext().getRealPath(xlsFolder);
        JSONObject json= new JSONObject();
        String provincia = request.getParameter("provincia");
        //System.err.println(provincia+" sdfsdfsdf");
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        LinkedList<ReportPrescrizioneFarmaci> listaPrescrizioniFarmaci = new LinkedList<ReportPrescrizioneFarmaci>();
        //String provincia = "Verona";
        System.err.println("Qualcuno");
        try {
            listaPrescrizioniFarmaci = (LinkedList<ReportPrescrizioneFarmaci>) sspDAO.getRicette(provincia);
            int rowCount = 0;
            Row row1 = sheet.createRow(rowCount++);
            
            
            Cell cell = row1.createCell(0);
            cell.setCellValue("DATA");

            cell = row1.createCell(1);
            cell.setCellValue("NOME FARMACO");

            cell = row1.createCell(2);
            cell.setCellValue("COSTO FARMACO");

            cell = row1.createCell(3);
            cell.setCellValue("MEDICO");

            cell = row1.createCell(4);
            cell.setCellValue("PAZIENTE");

            for (ReportPrescrizioneFarmaci a : listaPrescrizioniFarmaci) {
                Row row = sheet.createRow(rowCount++);
                writeCell(a, row);
            }
            String nomeXLS=provincia+"Report.xls";
            try ( FileOutputStream outputStream = new FileOutputStream(xlsFolder+"/"+nomeXLS)) {
                workbook.write(outputStream);
                
                json= new JSONObject();
                    json.putOpt("path", nomeXLS);
            } catch (Exception e) {
                System.err.println("Errore" + e);
            }
            
            
        } catch (DAOException ex) {
            Logger.getLogger(XlsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

    private void writeCell(ReportPrescrizioneFarmaci a, Row row) {
        Cell cell = row.createCell(0);
        cell.setCellValue(a.getData());

        cell = row.createCell(1);
        cell.setCellValue(a.getNomeFarmaco());

        cell = row.createCell(2);
        cell.setCellValue(a.getCostoFarmaco()+"€");

        cell = row.createCell(3);
        cell.setCellValue(a.getNomeCognomeMedico());

        cell = row.createCell(4);
        cell.setCellValue(a.getNomeCognomePaziente());
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String descrizione = request.getParameter("descrizione");
        String idMedico = request.getParameter("idMedico");
        String codFiscPaziente = request.getParameter("codiceFiscale");
        try {
            int idPaziente = userDao.getByCodiceFiscale(codFiscPaziente);
            //chiamare dao per creare la visita
        } catch (Exception e) {
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
