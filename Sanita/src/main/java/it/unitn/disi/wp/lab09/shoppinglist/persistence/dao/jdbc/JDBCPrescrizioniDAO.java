/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrescrizioniDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.DatiRicettaFarmaci;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Prescrizione;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author miche
 */
public class JDBCPrescrizioniDAO extends JDBCDAO<Prescrizione, Integer> implements PrescrizioniDAO{

    public JDBCPrescrizioniDAO(Connection con) {
        super(con);
    }
    
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Prescrizione getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Prescrizione> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<Prescrizione> getAllPrescrizioniEsami(int idPaziente) throws DAOException {
        
        LinkedList<Prescrizione> prescrizioni= new LinkedList<>();
        
        //Cerco le prescrizioni fatte al paziente idPaziente dal medico di base
        String query="select p.prenotato as prenotato, e.id as idEsame, p.id as id, p.descrizione as descrizione, b.nome as nome_medico, b.cognome as cognome_medico, pa.nome as nome_paziente, pa.cognome as cognome_paziente, e.nome as esame, p.visualizzato as visualizzato, p.data_prescrizione as dataPrescrizione, e.specializzazione as specializzazione, e.specializzazione as specializzazione\n" +
                        "from prescrizione p join medico_di_base b on p.medico=b.id \n" +
                        "        join paziente pa on p.paziente=pa.id \n" +
                        "        join esame e on p.esame=e.id\n" +
                        "where pa.id= "+idPaziente+" and farmaco is null";
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {
                    
                    
                    while (rs.next()) {
                        Prescrizione prescrizione = new Prescrizione();
                        
                        prescrizione.setId(rs.getInt("id"));
                        
                        prescrizione.setDescrizione(rs.getString("descrizione"));
                        prescrizione.setNomeMedico(rs.getString("nome_medico"));
                        prescrizione.setCognomeMedico(rs.getString("cognome_medico"));
                        prescrizione.setNomePaziente(rs.getString("nome_paziente"));
                        prescrizione.setCognomePaziente(rs.getString("cognome_paziente"));
                        prescrizione.setEsame(rs.getString("esame"));
                        prescrizione.setIdEsame(rs.getInt("idEsame"));
                        prescrizione.setVisualizzato(rs.getBoolean("visualizzato"));
                        prescrizione.setDataPrescrizione(rs.getString("dataPrescrizione"));
                        prescrizione.setSpecializzazioneEsame(rs.getInt("specializzazione"));
                        prescrizione.setPrenotato(rs.getBoolean("prenotato"));
                        System.err.println(rs.getBoolean("prenotato"));

                        prescrizioni.add(prescrizione);
                    }
                //System.err.println(new Gson().toJson(prescrizioni));
            } catch (SQLException ex){
                throw new DAOException("Impossible to get the list of Prescrizioni", ex);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of Prescrizioni", ex);
        }
        
        //Cerco le prescrizioni fatte a idPaziente dai medici Specialisti
        /*query="select p.id as id, p.descrizione as descrizione, b.nome as nome_medico, b.cognome as cognome_medico, pa.nome as nome_paziente, pa.cognome as cognome_paziente, e.nome as esame, p.visualizzato as visualizzato, p.data_prescrizione as dataPrescrizione\n" +
                        "from prescrizione p join medico_specialista b on p.medico=b.id \n" +
                        "        join paziente pa on p.paziente=pa.id \n" +
                        "        join esame e on p.esame=e.id\n" +
                        "where pa.id= "+idPaziente+" and farmaco is null";
                    
                    try (Statement stm1 = CON.createStatement()){
                        try (ResultSet rs1 = stm1.executeQuery(query)){
                            while (rs1.next()) {
                                Prescrizione prescrizione = new Prescrizione();
                                prescrizione.setId(rs1.getInt("id"));
                                prescrizione.setDescrizione(rs1.getString("descrizione"));
                                prescrizione.setNomeMedico(rs1.getString("nome_medico"));
                                prescrizione.setCognomeMedico(rs1.getString("cognome_medico"));
                                prescrizione.setNomePaziente(rs1.getString("nome_paziente"));
                                prescrizione.setCognomePaziente(rs1.getString("cognome_paziente"));
                                prescrizione.setEsame(rs1.getString("esame"));
                                prescrizione.setVisualizzato(rs1.getBoolean("visualizzato"));
                                prescrizione.setDataPrescrizione(rs1.getString("dataPrescrizione"));


                                prescrizioni.add(prescrizione);
                            }
                            
                        } catch (SQLException e) {
                            throw new DAOException("Impossible to get the list of Prescrizione", e);
                        }
                    } catch (SQLException e) {
                        throw new DAOException("Impossible to get the list of Prescrizione", e);
                    }
        */
        
        
          
        
        return prescrizioni;
    }

    @Override
    public LinkedList<Prescrizione> getAllPrescrizioniFarmaci(int idPaziente) throws DAOException {
        
        
        LinkedList<Prescrizione> prescrizioni= new LinkedList<>();
        
        //Cerco le prescrizioni fatte al paziente idPaziente dal medico di base
        String query="select p.id as id, p.descrizione as descrizione, b.nome as nome_medico, b.cognome as cognome_medico, pa.nome as nome_paziente, pa.cognome as cognome_paziente, f.nome as farmaco, p.visualizzato as visualizzato, p.data_prescrizione as dataPrescrizione\n" +
                        "from prescrizione p join medico_di_base b on p.medico=b.id \n" +
                        "        join paziente pa on p.paziente=pa.id \n" +
                        "        join farmaco f on p.farmaco=f.id\n" +
                        "where pa.id= "+idPaziente+" and esame is null";
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(query)) {
                
                    while (rs.next()) {
                        Prescrizione prescrizione = new Prescrizione();
                        
                        prescrizione.setId(rs.getInt("id"));
                        
                        prescrizione.setDescrizione(rs.getString("descrizione"));
                        prescrizione.setNomeMedico(rs.getString("nome_medico"));
                        prescrizione.setCognomeMedico(rs.getString("cognome_medico"));
                        prescrizione.setNomePaziente(rs.getString("nome_paziente"));
                        prescrizione.setCognomePaziente(rs.getString("cognome_paziente"));
                        prescrizione.setFarmaco(rs.getString("farmaco"));
                        prescrizione.setVisualizzato(rs.getBoolean("visualizzato"));
                        prescrizione.setDataPrescrizione(rs.getString("dataPrescrizione"));


                        prescrizioni.add(prescrizione);
                    }
                
            } catch (SQLException ex){
                throw new DAOException("Impossible to get the list of Prescrizioni", ex);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of Prescrizioni", ex);
        }
        
        //Cerco le prescrizioni fatte a idPaziente dai medici Specialisti
        query="select p.id as id, p.descrizione as descrizione, b.nome as nome_medico, b.cognome as cognome_medico, pa.nome as nome_paziente, pa.cognome as cognome_paziente, f.nome as farmaco, p.visualizzato as visualizzato, p.data_prescrizione as dataPrescrizione\n" +
                        "from prescrizione p join medico_specialista b on p.medico=b.id \n" +
                        "        join paziente pa on p.paziente=pa.id \n" +
                        "        join farmaco f on p.farmaco=f.id\n" +
                        "where pa.id= "+idPaziente+" and esame is null";
                    
                    try (Statement stm1 = CON.createStatement()){
                        try (ResultSet rs1 = stm1.executeQuery(query)){
                            while (rs1.next()) {
                                Prescrizione prescrizione = new Prescrizione();
                                prescrizione.setId(rs1.getInt("id"));
                                prescrizione.setDescrizione(rs1.getString("descrizione"));
                                prescrizione.setNomeMedico(rs1.getString("nome_medico"));
                                prescrizione.setCognomeMedico(rs1.getString("cognome_medico"));
                                prescrizione.setNomePaziente(rs1.getString("nome_paziente"));
                                prescrizione.setCognomePaziente(rs1.getString("cognome_paziente"));
                                prescrizione.setFarmaco(rs1.getString("farmaco"));
                                prescrizione.setVisualizzato(rs1.getBoolean("visualizzato"));
                                prescrizione.setDataPrescrizione(rs1.getString("dataPrescrizione"));


                                prescrizioni.add(prescrizione);
                            }
                            
                        } catch (SQLException e) {
                            throw new DAOException("Impossible to get the list of Prescrizione", e);
                        }
                    } catch (SQLException e) {
                        throw new DAOException("Impossible to get the list of Prescrizione", e);
                    }
        
          
        
        return prescrizioni;
    }
    
    
    @Override
    public Prescrizione getByID(int id) throws DAOException {
        Prescrizione prescrizione=new Prescrizione();
        String query= "SELECT * FROM PRESCRIZIONE PR JOIN PAZIENTE ON PR.PAZIENTE=PA.ID WHERE PR.ID="+id;
            try (Statement stm1 = CON.createStatement()){
                try (ResultSet rs1 = stm1.executeQuery(query)){
                    prescrizione.setId(rs1.getInt("id"));
                    prescrizione.setDescrizione(rs1.getString("descrizione"));
                    prescrizione.setNomePaziente(rs1.getString("nome_paziente"));
                    prescrizione.setCognomePaziente(rs1.getString("cognome_paziente"));
                    prescrizione.setCodiceFiscalePaziente(rs1.getString("codice_fiscale"));
                    prescrizione.setFarmaco(rs1.getString("farmaco"));
                    prescrizione.setVisualizzato(rs1.getBoolean("visualizzato"));
                    prescrizione.setDataPrescrizione(rs1.getString("dataPrescrizione"));
                }catch(Exception e){
                    
                }
                
            }catch(Exception e){
                
            }
        return prescrizione;
    }

    @Override
    public void setPrenotato(int id) throws DAOException {
        System.err.println("Sono stato chiamato id: "+id);
        try {
            PreparedStatement std1 = CON.prepareStatement("update PRESCRIZIONE set prenotato=true where id="+id);
            std1.executeUpdate();
        } catch (SQLException ex) {
            System.err.println("Non sono riuscito a fare l'update: "+ex);
        }
    }

    @Override
    public DatiRicettaFarmaci getDatiRicetta(int id) throws DAOException {
        DatiRicettaFarmaci ricetta=new DatiRicettaFarmaci();
        String query= "select p.medico as codice_medico,paz.codice_fiscale as codice_fiscale, p.data_prescrizione as data_prescrizione, p.id as id_prescrizione,f.descrizione as descrizione_farmaco from prescrizione p join paziente paz on p.paziente=paz.id join farmaco f on p.farmaco=f.id where p.id="+id;
            try (Statement stm = CON.createStatement()){
                try (ResultSet rs = stm.executeQuery(query)){
                    if(rs.next()){
                        ricetta.setCodiceMedico(rs.getInt("codice_medico"));
                        ricetta.setCodiceFiscalePaziente(rs.getString("codice_fiscale"));
                        ricetta.setDataPrescrizione(rs.getString("data_prescrizione"));
                        ricetta.setCodicePrescrizione(rs.getInt("id_prescrizione"));
                        ricetta.setDescrizioneFarmaco(rs.getString("descrizione_farmaco"));
                    }
                }catch(Exception e){
                    System.err.println(e);
                }
                
            }catch(Exception e){
                System.err.println(e);
            }
        return ricetta;
    }

    @Override
    public void setFarmaco(int id, int id_farmaco, int medico, String desc) throws DAOException{
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String data = dtf.format(now);
        String query = "INSERT INTO PRESCRIZIONE (PAZIENTE, MEDICO, FARMACO, DATA_PRESCRIZIONE,"
                        +"VISUALIZZATO,PRENOTATO, DESCRIZIONE)"+ 
                        "VALUES ("+id+", "+medico+", "+id_farmaco+", '"+data+"',false,false,'"+desc+"')";
        try (Statement stm = CON.createStatement()){
            stm.executeUpdate(query);
        }catch (SQLException ex) {
                
                System.err.println(ex);
        }
    }

    @Override
    public void setEsame(int id, int id_esame, int medico, String desc) throws DAOException{
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String data = dtf.format(now);
        String query = "INSERT INTO PRESCRIZIONE (PAZIENTE, MEDICO, ESAME, DATA_PRESCRIZIONE,"
                        +"VISUALIZZATO,PRENOTATO, DESCRIZIONE)"+ 
                        "VALUES ("+id+", "+medico+", "+id_esame+", '"+data+"',false,false,'"+desc+"')";
        try (Statement stm = CON.createStatement()){
            stm.executeUpdate(query);
        }catch (SQLException ex) {
                throw new DAOException("Errore: "+ ex, ex);
        }
    }

    @Override
    public void setVisita(int id, int medico, String desc) throws DAOException{
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String data = dtf.format(now);
        String query = "INSERT INTO VISITA_MEDICO_DI_BASE (ID_PAZIENTE, ID_MEDICO, DATA_VISITA, DESCRIZIONE)"+ 
                        "VALUES ("+id+", "+medico+", '"+data+"','"+desc+"')";
        try (Statement stm = CON.createStatement()){
            stm.executeUpdate(query);
        }catch (SQLException ex) {
                throw new DAOException("Errore: "+ ex, ex);
        }
    }
    
}
