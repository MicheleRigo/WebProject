/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.EsamePaziente;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.EsamePazienteDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ObjectPerAutocompletamento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author miche
 */
public class JDBCEsamePazienteDAO extends JDBCDAO<EsamePaziente, Integer> implements EsamePazienteDAO{

    public JDBCEsamePazienteDAO(Connection con){
        super(con);
    }
    
    
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EsamePaziente getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EsamePaziente> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<EsamePaziente> getEsamePaziente(int idPaziente) throws DAOException {
        LinkedList<EsamePaziente> esamiPaziente = new LinkedList<>();

        /*String query = " " ;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {

                while (rs.next()) {
                    EsamePaziente esame = new EsamePaziente();

                    esame.setId(rs.getInt("v.id"));

                    /*
                        private int id;
                        private int prenotazione;
                        private String esito;
                        private int idSpecialista;
                        private int idSSP;
                        private int idEsame;//id della tabella degli esami che possono essere fatti
                        private int idPaziente;
                        private String data;
                        private String ora;

                        private String nomeSpecialista;
                        private String cognomeSpecialista;
                        private String nomeSSP;
                        private String nomeEsame;
                    esamiPaziente.add(esame);
                }

            } catch (SQLException ex) {
                System.err.println("Impossible to get the list of Visita" + ex);
                throw new DAOException("Impossible to get the list of Visita", ex);
            }
        } catch (SQLException ex) {
            System.err.println("Impossible to get the list of Visita" + ex);
            throw new DAOException("Impossible to get the list of Visita", ex);
        }*/

        return esamiPaziente;
    }

    @Override
    public List getEsameAutocompletamento(String string) throws DAOException {
        String query = "select nome from esame where ucase(nome) LIKE '%"+string+"%'";

        try (PreparedStatement stm = CON.prepareStatement(query)) {
            
            try (ResultSet rs = stm.executeQuery()) {
                

                List<ObjectPerAutocompletamento> pazienti = new ArrayList<ObjectPerAutocompletamento>();
                int i=1;
                while (rs.next()) {
                    ObjectPerAutocompletamento o = new ObjectPerAutocompletamento();
                    o.setId(i);
                    o.setText(rs.getString("nome").toUpperCase());
                    //System.err.println("Ho trovato qualcosa");
                    
                    //pazienti.add(rs.getString("nome")+" "+rs.getString("cognome")+" "+rs.getString("data_di_nascita")+" "+rs.getString("codice_fiscale"));
                    pazienti.add(o);
                    i++;
                }
                //System.err.println(pazienti.size());
                return pazienti;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            throw new DAOException("Impossible to get the list of esami", ex);
        }
    }

    @Override
    public int getIdEsameByName(String nome) throws DAOException {
        String query = "SELECT ID FROM ESAME WHERE ucase(NOME)='" + nome.toUpperCase() + "'";
        int id = 0;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {
                while (rs.next()) {
                    id = Integer.parseInt(rs.getString("id"));
                }

            }
        } catch (SQLException ex) {
            throw new DAOException("Impossibile trovare l'esame: ", ex);
        }

        return id;
    }
    
}
