/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente;

/**
 *
 * @author miche
 */
public interface CookieDAO extends DAO<Utente,Integer>{
    
    public void addCookie(String valore, int utente);
    
    public int getUtente(String valore);
    
    public void deleteCookie(String valore);
    
}
