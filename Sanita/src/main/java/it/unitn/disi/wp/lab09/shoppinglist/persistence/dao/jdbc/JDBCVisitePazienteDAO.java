/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.VisitePazienteDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Visita;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author miche
 */
public class JDBCVisitePazienteDAO extends JDBCDAO<Visita, Integer> implements VisitePazienteDAO {

    public JDBCVisitePazienteDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Visita getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Visita> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<Visita> getVisitePaziente(int idPaziente) throws DAOException {
        LinkedList<Visita> visite = new LinkedList<>();

        String query = "SELECT v.id, v.data_visita, v.paziente, v.esame, v.esito, v.id_Medico, v.prenotazione, e.nome as nome_esame, m.nome as nome_medico, m.cognome as cognome_medico  FROM VISITA V JOIN ESAME E ON V.ESAME=E.ID JOIN medico_specialista m on v.id_medico=m.id WHERE PAZIENTE=" + idPaziente;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {

                while (rs.next()) {
                    Visita visita = new Visita();

                    visita.setId(rs.getInt("id"));

                    visita.setData(rs.getString("data_visita"));
                    visita.setIdPaziente(rs.getInt("paziente"));
                    visita.setIdEsame(rs.getInt("esame"));
                    visita.setEsito(rs.getString("esito"));
                    visita.setIdMedicoCheFaEsame(rs.getInt("id_medico"));
                    visita.setIdPrenotazione(rs.getInt("prenotazione"));
                    visita.setNomeEsame(rs.getString("nome_esame"));
                    visita.setNomeMedico(rs.getString("nome_medico") + " " + rs.getString("cognome_medico"));

                    visite.add(visita);
                }

            } catch (SQLException ex) {
                System.err.println("Impossible to get the list of Visita" + ex);
                throw new DAOException("Impossible to get the list of Visita", ex);
            }
        } catch (SQLException ex) {
            System.err.println("Impossible to get the list of Visita" + ex);
            throw new DAOException("Impossible to get the list of Visita", ex);
        }

        query = "SELECT v.id, v.data_visita, v.paziente, v.esame, v.esito, v.id_Medico, v.prenotazione, e.nome as nome_esame, s.provincia as nome_medico  FROM VISITA V JOIN ESAME E ON V.ESAME=E.ID JOIN servizio_sanitario s on v.id_medico=s.id WHERE PAZIENTE="+ idPaziente;
        try ( Statement stm1 = CON.createStatement()) {
            try ( ResultSet rs1 = stm1.executeQuery(query)) {

                while (rs1.next()) {
                    Visita visita = new Visita();

                    visita.setId(rs1.getInt("id"));

                    visita.setData(rs1.getString("data_visita"));
                    visita.setIdPaziente(rs1.getInt("paziente"));
                    visita.setIdEsame(rs1.getInt("esame"));
                    visita.setEsito(rs1.getString("esito"));
                    visita.setIdMedicoCheFaEsame(rs1.getInt("id_medico"));
                    visita.setIdPrenotazione(rs1.getInt("prenotazione"));
                    visita.setNomeEsame(rs1.getString("nome_esame"));
                    visita.setNomeMedico("SSP "+rs1.getString("nome_medico"));

                    visite.add(visita);
                }

            } catch (SQLException ex) {
                System.err.println("Impossible to get the list of Visita" + ex);
                throw new DAOException("Impossible to get the list of Visita", ex);
            }
        } catch (SQLException ex) {
            System.err.println("Impossible to get the list of Visita" + ex);
            throw new DAOException("Impossible to get the list of Visita", ex);
        }

        return visite;
    }

    @Override
    public void insertVisita(int idPaziente, int idEsame, String esito, String idMedico) throws DAOException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String dataVisita = formatter.format(date).toString();

        try {
            PreparedStatement std1 = CON.prepareStatement("insert into visita (paziente, esame, esito, id_medico, data_visita, prenotazione)values (" + idPaziente + "," + idEsame + ",'" + esito + "'," + idMedico + ",'" + dataVisita + "',null) ");
            std1.executeUpdate();
        } catch (Exception e) {
            System.err.println("Errore Insert Visita " + e);
        }
    }

}
