/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.EsamePaziente;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author miche
 */
public interface EsamePazienteDAO extends DAO<EsamePaziente, Integer>{
    
    public LinkedList<EsamePaziente> getEsamePaziente(int idPaziente) throws DAOException;
    
    public List getEsameAutocompletamento(String string) throws DAOException;
    
    public int getIdEsameByName(String nome) throws DAOException;
}
