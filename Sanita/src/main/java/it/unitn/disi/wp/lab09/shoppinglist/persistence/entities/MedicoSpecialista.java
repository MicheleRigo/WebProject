/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author miche
 */
public class MedicoSpecialista extends Utente{

    private String nome;
    private String cognome;
    private String sesso;
    private String viaStudio;
    private String numeroCivicoStudio;
    private String capStudio;
    private String specializzazione;
    private String provincia;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getViaStudio() {
        return viaStudio;
    }

    public void setViaStudio(String viaStudio) {
        this.viaStudio = viaStudio;
    }

    public String getNumeroCivicoStudio() {
        return numeroCivicoStudio;
    }

    public void setNumeroCivicoStudio(String numeroCivicoStudio) {
        this.numeroCivicoStudio = numeroCivicoStudio;
    }

    public String getCapStudio() {
        return capStudio;
    }

    public void setCapStudio(String capStudio) {
        this.capStudio = capStudio;
    }

    public String getSpecializzazione() {
        return specializzazione;
    }

    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

        
    
    
    public String tipoUtente(){
        return "MedicoSpecialista";
    }
    
}
