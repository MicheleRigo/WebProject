/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.CookieDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author Jacopo
 */
public class JDBCCookieDAO extends JDBCDAO<Utente,Integer> implements CookieDAO{
    
    public JDBCCookieDAO(Connection con) {
        super(con);
        
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Utente getByPrimaryKey(Integer primaryKey) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Utente> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCookie(String valore, int utente) {//controlla se ci sono cookie con lo stesso valore e poi inserisce il nuovo cookie
        String query = "SELECT utente FROM cookies WHERE valore='" + valore + "'";
        int cont=0;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {
                while (rs.next()) {
                    cont++;
                }

            }
        } catch (Exception ex) {
            System.out.println("Non sono riuscito a trovare l'utente di questo cookie"+ex.toString());
        }
        if(cont>0){//esistono già cookie con questo valore, bisogna eliminarli prima di inserirne un altro
            System.out.println("Trovati cookie già esistenti di questo utente, rimozione dal database in corso...");
            query = "DELETE FROM cookies WHERE utente="+utente;
            try{
                PreparedStatement ps=CON.prepareStatement(query);
                ps.executeUpdate();
            }
            catch(Exception e){
                System.out.println("Errore nella rimozione dei cookie");
            }
        }
        else{
            System.out.println("Non sono stati trovati cookie già esistenti con questo valore");
        }
        try{
            PreparedStatement ps=CON.prepareStatement("insert into cookies(valore,utente) values ('"+valore+"', +"+utente+")");
            ps.executeUpdate();
        }
        catch(Exception e){
            System.out.println("Non sono riuscito ad inserire il cookie nel database"+e.toString());
        }
    }

    @Override
    public int getUtente(String valore) {
        String query = "SELECT utente FROM cookies WHERE valore='" + valore + "'";
        int id = -1;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {
                while (rs.next()) {
                    id = Integer.parseInt(rs.getString("utente"));
                }

            }
        } catch (Exception ex) {
            System.out.println("Non sono riuscito a trovare l'utente di questo cookie"+ex.toString());
        }

        return id;
    }

    @Override
    public void deleteCookie(String valore) {
        String query = "DELETE FROM cookies WHERE valore='"+valore+"'";
            try{
                PreparedStatement ps=CON.prepareStatement(query);
                ps.executeUpdate();
                System.out.println("Cancellato cookie"+valore);
            }
            catch(Exception e){
                System.out.println("Errore nella rimozione del cookie "+e);
            }
    }
    
}
