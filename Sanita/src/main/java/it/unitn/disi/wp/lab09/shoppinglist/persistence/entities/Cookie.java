/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author Jacopo
 */
public class Cookie {
    private int id;
    private String valore;
    private int utente;

    public Cookie(int id, String valore, int utente) {
        this.id = id;
        this.valore = valore;
        this.utente = utente;
    }
    
    public int getId() {
        return id;
    }

    public int getUtente() {
        return utente;
    }

    public String getValore() {
        return valore;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUtente(int utente) {
        this.utente = utente;
    }

    public void setValore(String valore) {
        this.valore = valore;
    }
    
    
}
