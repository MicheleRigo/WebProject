/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.json.JSONObject;

/**
 *
 * @author miche
 */
@MultipartConfig
public class CambiaFotoServlet extends HttpServlet {

    private UserDAO userDao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @throws ServletException if a servlet-specific error occurs
     */
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String avatarsFolder = getServletContext().getInitParameter("avatarsFolder");
        if (avatarsFolder == null) {
            System.err.println("non trovata pdfFolder");
        }

        avatarsFolder = getServletContext().getRealPath(avatarsFolder);

        int id = Integer.parseInt(request.getParameter("id"));
        System.err.println("Id: " + id);
        JSONObject json= new JSONObject();
        try {
            Paziente paziente = userDao.getPazienteById(id);
            Part filePart = request.getPart("avatar");
            //System.err.println(filePart);

            if ((filePart != null) && (filePart.getSize() > 0)) {
                String filename = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();//MSIE  fix.
                System.err.println("Filename: " + filename);
                try ( InputStream fileContent = filePart.getInputStream()) {

                    File file = new File(avatarsFolder, filename);

                    Files.copy(fileContent, file.toPath());
                    if (!filename.equals(paziente.getFoto())) {
                        paziente.setFoto(filename);
                        
                        userDao.setFotoPaziente(id, filename);
                        json = new JSONObject();
                        json.putOpt("path", filename);
                        
                    }
                } catch (FileAlreadyExistsException ex) {
                    userDao.setFotoPaziente(id, filename);
                    json = new JSONObject();
                        json.putOpt("path", filename);
                    getServletContext().log("File \"" + filename + "\" already exists on the server");
                    System.err.println("Errore " + ex);
                } catch (RuntimeException ex) {
                    //TODO: handle the exception
                    getServletContext().log("impossible to upload the file", ex);
                    System.err.println("Errore " + ex);
                }

            } else {
                System.err.println("Problem");
            }

        } catch (DAOException ex) {
            System.err.println(ex);
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void dispatch(javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response, String nextPage)
            throws ServletException, IOException {

        RequestDispatcher dispatch = request.getRequestDispatcher(nextPage);
        dispatch.forward(request, response);

    }

}
