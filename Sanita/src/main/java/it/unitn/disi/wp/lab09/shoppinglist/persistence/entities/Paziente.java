/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

import java.util.LinkedList;

/**
 *
 * @author miche
 */
public class Paziente extends Utente {

    private String nome;
    private String cognome;
    private String sesso;
    private String foto;
    private String codiceFiscale;
    private String dataDiNascita;
    private String luogoDiNascita;
    private String provincia;
    private MedicoDiBase medicoDiBase;
    

   

    public MedicoDiBase getMedicoDiBase() {
        return medicoDiBase;
    }

    public void setMedicoDiBase(MedicoDiBase medicoDiBase) {
        this.medicoDiBase = medicoDiBase;
    }

   
  
    

    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getDataDiNascita() {
        return dataDiNascita;
    }

    public void setDataDiNascita(String dataDiNascita) {
        this.dataDiNascita = dataDiNascita;
    }

    public String getLuogoDiNascita() {
        return luogoDiNascita;
    }

    public void setLuogoDiNascita(String luogoDiNascita) {
        this.luogoDiNascita = luogoDiNascita;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String tipoUtente() {
        return "Paziente";
    }
}
