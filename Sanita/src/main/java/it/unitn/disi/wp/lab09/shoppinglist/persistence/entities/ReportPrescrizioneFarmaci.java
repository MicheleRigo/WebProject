/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author miche
 */
public class ReportPrescrizioneFarmaci {
    private String data;
    private String nomeFarmaco;
    private String nomeCognomeMedico;
    private String nomeCognomePaziente;
    private double costoFarmaco;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNomeFarmaco() {
        return nomeFarmaco;
    }

    public void setNomeFarmaco(String nomeFarmaco) {
        this.nomeFarmaco = nomeFarmaco;
    }

    public String getNomeCognomeMedico() {
        return nomeCognomeMedico;
    }

    public void setNomeCognomeMedico(String nomeCognomeMedico) {
        this.nomeCognomeMedico = nomeCognomeMedico;
    }

    public String getNomeCognomePaziente() {
        return nomeCognomePaziente;
    }

    public void setNomeCognomePaziente(String nomeCognomePaziente) {
        this.nomeCognomePaziente = nomeCognomePaziente;
    }

    public double getCostoFarmaco() {
        return costoFarmaco;
    }

    public void setCostoFarmaco(double costoFarmaco) {
        this.costoFarmaco = costoFarmaco;
    }
    
    
    
    
}
