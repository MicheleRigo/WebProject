/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.VisitaMedicoDiBase;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.VisiteMedicoDiBasePazienteDAO;

/**
 *
 * @author miche
 */
public class JDBCVisiteMedicoDiBasePazienteDAO extends JDBCDAO<VisitaMedicoDiBase, Integer> implements VisiteMedicoDiBasePazienteDAO{

    public JDBCVisiteMedicoDiBasePazienteDAO(Connection con){
        super(con);
    }
    
    
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VisitaMedicoDiBase getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<VisitaMedicoDiBase> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<VisitaMedicoDiBase> getVisiteMedicoDiBasePaziente(int idPaziente) throws DAOException {
        LinkedList<VisitaMedicoDiBase> visiteMedicoDiBase = new LinkedList<>();

        String query = "SELECT v.id,v.data_visita,v.descrizione,v.id_medico,m.nome,m.cognome,v.id_paziente FROM VISITA_MEDICO_DI_BASE V JOIN MEDICO_DI_BASE M ON V.ID_MEDICO=M.ID WHERE V.ID_PAZIENTE=" + idPaziente;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {

                while (rs.next()) {
                    VisitaMedicoDiBase visita = new VisitaMedicoDiBase();

                    visita.setId(rs.getInt("id"));
                    visita.setDataVisita(rs.getString("data_visita"));
                    visita.setDescrizione(rs.getString("descrizione"));
                    visita.setIdMedico(rs.getString("id_medico"));
                    visita.setNomeMedico(rs.getString("nome"));
                    visita.setCognomeMedico(rs.getString("cognome"));
                    visita.setIdPaziente(rs.getInt("id_paziente"));

                    /*id;
    private String dataVisita;
    private String descrizione;
    private String idMedico;
    private String nomeMedico;
    private String cognomeMedico;
    private int idPaziente;*/
                    visiteMedicoDiBase.add(visita);
                }

            } catch (SQLException ex) {
                throw new DAOException("Impossible to get the list of VisiteMedicoDiBase", ex);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of VisiteMedicoDiBase", ex);
        }

        return visiteMedicoDiBase;
    }
    
}
