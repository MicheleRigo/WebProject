/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 09 - Shopping List Implementation
 * UniTN
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.jdbc;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoSpecialista;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ObjectPerAutocompletamento;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ServizioSanitarioProvinciale;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * The JDBC implementation of the {@link UserDAO} interface.
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public class JDBCUserDAO extends JDBCDAO<Utente, Integer> implements UserDAO {

    /**
     * The default constructor of the class.
     *
     * @param con the connection to the persistence system.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    public JDBCUserDAO(Connection con) {
        super(con);
    }

    /**
     * Returns the number of {@link User users} stored on the persistence system
     * of the application.
     *
     * @return the number of records present into the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM utente");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }

        return 0L;
    }

    /**
     * Returns the {@link User user} with the primary key equals to the one
     * passed as parameter.
     *
     * @param primaryKey the {@code id} of the {@code user} to get.
     * @return the {@code user} with the id equals to the one passed as
     * parameter or {@code null} if no entities with that id are present into
     * the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public Utente getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        //Interrogo il db cercando l'utente con i mail e password che mi sono stati mandati
        try {
            /*(PreparedStatement stm = CON.prepareStatement("SELECT * FROM utente WHERE mail = ? AND password = ?"))
            stm.setString(1, email);
            stm.setString(2, password);*/
            String query;
            Statement statement = CON.createStatement();
            ResultSet rs;
            query = "SELECT id FROM utente WHERE id = " + primaryKey;

            try {
                /*(ResultSet rs = stm.executeQuery())*/
                rs = statement.executeQuery(query);

                int count = 0;
                while (rs.next()) {
                    count++;
                }
                if (count > 1) {//Se count>1 vuol dire che ho due o più utenti uguali
                    throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                } else {

                    //Controllo se l'utente è un medico di base
                    query = "SELECT * FROM utente U join medico_di_base M on U.id=M.id WHERE U.id = " + primaryKey;

                    rs = statement.executeQuery(query);

                    if (rs.next()) {

                        //l'utente è un medico di base
                        //Setto i dati che ho ricavato in un oggetto Medico di base
                        MedicoDiBase user = new MedicoDiBase();

                        //Parametri utente
                        user.setId(rs.getInt("id"));
                        user.setEmail(rs.getString("mail"));
                        user.setPassword(rs.getString("password"));

                        //Parametri medico di base
                        user.setNome(rs.getString("nome"));
                        user.setCognome(rs.getString("cognome"));
                        user.setCapStudio(rs.getString("cap"));
                        user.setNumeroCivicoStudio(rs.getString("civico"));
                        user.setViaStudio(rs.getString("via"));
                        user.setSesso(rs.getString("sesso"));
                        user.setProvincia(rs.getString("provincia"));
                        
                        
                        
                        

                        return user;
                    } else {
                        ResultSet rs1;
                        Statement statement1 = CON.createStatement();
                        query = "SELECT U.id, U.mail, U.password, M.cap, M.civico, M.via, M.nome as nome, M.cognome, M.sesso, M.provincia, s.nome as specializzazione FROM utente U join medico_specialista M on U.id=M.id join specializzazione s on M.specializzazione = s.id WHERE U.id = " + primaryKey;

                        rs1 = statement1.executeQuery(query);

                        if (rs1.next()) {
                            //
                            //l'utente è un medico Specialista
                            //Setto i dati che ho ricavato in un oggetto Medico Specialista
                            MedicoSpecialista user = new MedicoSpecialista();

                            //Parametri utente
                            user.setId(rs1.getInt("id"));
                            user.setEmail(rs1.getString("mail"));
                            user.setPassword(rs1.getString("password"));

                            //Parametri medico specialista
                            user.setCapStudio(rs1.getString("cap"));
                            user.setNumeroCivicoStudio(rs1.getString("civico"));
                            user.setViaStudio(rs1.getString("via"));
                            user.setNome(rs1.getString("nome"));
                            user.setCognome(rs1.getString("cognome"));
                            user.setSesso(rs1.getString("sesso"));
                            user.setProvincia(rs1.getString("provincia"));
                            user.setSpecializzazione(rs1.getString("specializzazione"));
                            System.err.println(user.getSpecializzazione());

                            return user;
                        } else {

                            ResultSet res;
                            Statement state = CON.createStatement();
                            query = "SELECT * FROM utente U join paziente p on U.id=p.id WHERE U.id = " + primaryKey;

                            res = state.executeQuery(query);

                            //L'utente è un semplice utente
                            //Setto i dati che ho ricavato in un oggetto Utente
                            if (res.next()) {
                                Paziente user = new Paziente();
                                user.setId(res.getInt("id"));
                                user.setNome(res.getString("nome"));
                                user.setCognome(res.getString("cognome"));
                                user.setEmail(res.getString("mail"));
                                user.setPassword(res.getString("password"));
                                user.setSesso(res.getString("sesso"));
                                user.setCodiceFiscale(res.getString("codice_fiscale"));
                                user.setFoto(res.getString("foto"));
                                user.setDataDiNascita(res.getString("data_di_nascita"));
                                user.setLuogoDiNascita(res.getString("luogo_di_nascita"));
                                user.setProvincia(res.getString("provincia"));
                                return user;
                            } else {

                                ResultSet res1;
                                Statement state1 = CON.createStatement();
                                query = "SELECT * FROM utente U join servizio_sanitario s on U.id=s.id WHERE U.id = " + primaryKey;

                                res1 = state1.executeQuery(query);

                                //L'utente è un servizio sanitario
                                //Setto i dati che ho ricavato in un oggetto ServizioSanitarioProvinciale
                                if (res1.next()) {
                                    ServizioSanitarioProvinciale user = new ServizioSanitarioProvinciale();
                                    user.setId(res1.getInt("id"));
                                    user.setEmail(res1.getString("mail"));
                                    user.setPassword(res1.getString("password"));
                                    user.setVia(res1.getString("via"));
                                    user.setCap(res1.getString("cap"));
                                    user.setCivico(res1.getString("civico"));
                                    user.setProvincia(res1.getString("provincia"));
                                    return user;
                                }
                            }

                        }
                    }

                }

            } catch (SQLException ex) {
                System.err.println("Sono entrato nel catch.........");
                System.err.println(ex);

                //return null;
            }
        } catch (SQLException ex) {

            throw new DAOException("Impossible to get the list of users", ex);

        }
        return null;
    }

    /**
     * Returns the {@link User user} with the given {@code email} and
     * {@code password}.
     *
     * @param email the email of the user to get.
     * @param password the password of the user to get.
     * @return the {@link User user} with the given {@code email} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public Utente getByEmailAndPassword(String email, String password) throws DAOException {

        //Controllo che non mi siano state passate due variabili nulle
        if ((email == null) || (password == null)) {
            throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));
        }

        //Interrogo il db cercando l'utente con i mail e password che mi sono stati mandati
        try {
            String query;
            Statement statement = CON.createStatement();
            ResultSet rs;
            query = "SELECT id FROM utente WHERE mail = '" + email + "' AND password = '" + password + "'";

            try {

                rs = statement.executeQuery(query);

                int count = 0;
                while (rs.next()) {
                    count++;
                }
                if (count > 1) {//Se count>1 vuol dire che ho due o più utenti uguali
                    throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                } else {

                    //Controllo se l'utente è un medico di base
                    query = "SELECT * FROM utente U join medico_di_base M on U.id=M.id WHERE mail = '" + email + "' AND password = '" + password + "'";

                    rs = statement.executeQuery(query);

                    if (rs.next()) {

                        //l'utente è un medico di base
                        //Setto i dati che ho ricavato in un oggetto Medico di base
                        MedicoDiBase user = new MedicoDiBase();

                        //Parametri utente
                        user.setId(rs.getInt("id"));
                        user.setEmail(rs.getString("mail"));
                        user.setPassword(rs.getString("password"));

                        //Parametri medico di base
                        user.setNome(rs.getString("nome"));
                        user.setCognome(rs.getString("cognome"));
                        user.setCapStudio(rs.getString("cap"));
                        user.setNumeroCivicoStudio(rs.getString("civico"));
                        user.setViaStudio(rs.getString("via"));
                        user.setSesso(rs.getString("sesso"));
                        user.setProvincia(rs.getString("provincia"));
                        

                        return user;
                    } else {
                        ResultSet rs1;
                        Statement statement1 = CON.createStatement();
                        query = "SELECT U.id, U.mail, U.password, M.cap, M.civico, M.via, M.nome as nome, M.cognome, M.sesso, M.provincia, s.nome as specializzazione FROM utente U join medico_specialista M on U.id=M.id join specializzazione s on M.specializzazione = s.id WHERE mail = '" + email + "' AND password = '" + password + "'";

                        rs1 = statement1.executeQuery(query);

                        if (rs1.next()) {
                            //
                            //l'utente è un medico Specialista
                            //Setto i dati che ho ricavato in un oggetto Medico Specialista
                            MedicoSpecialista user = new MedicoSpecialista();

                            //Parametri utente
                            user.setId(rs1.getInt("id"));
                            user.setEmail(rs1.getString("mail"));
                            user.setPassword(rs1.getString("password"));

                            //Parametri medico specialista
                            user.setCapStudio(rs1.getString("cap"));
                            user.setNumeroCivicoStudio(rs1.getString("civico"));
                            user.setViaStudio(rs1.getString("via"));
                            user.setNome(rs1.getString("nome"));
                            user.setCognome(rs1.getString("cognome"));
                            user.setSesso(rs1.getString("sesso"));
                            user.setProvincia(rs1.getString("provincia"));
                            user.setSpecializzazione(rs1.getString("specializzazione"));
                            

                            return user;
                        } else {

                            ResultSet res;
                            Statement state = CON.createStatement();
                            query = "SELECT * FROM utente U join paziente p on U.id=p.id WHERE mail = '" + email + "' AND password = '" + password + "'";

                            res = state.executeQuery(query);

                            //L'utente è un Paziente
                            //Setto i dati che ho ricavato in un oggetto Utente
                            if (res.next()) {
                                Paziente user = new Paziente();
                                user.setId(res.getInt("id"));
                                user.setNome(res.getString("nome"));
                                user.setCognome(res.getString("cognome"));
                                user.setEmail(res.getString("mail"));
                                user.setPassword(res.getString("password"));
                                user.setSesso(res.getString("sesso"));
                                user.setCodiceFiscale(res.getString("codice_fiscale"));
                                user.setFoto(res.getString("foto"));
                                user.setDataDiNascita(res.getString("data_di_nascita"));
                                user.setLuogoDiNascita(res.getString("luogo_di_nascita"));
                                user.setProvincia(res.getString("provincia"));

                               
                                return user;
                            } else {

                                ResultSet res1;
                                Statement state1 = CON.createStatement();
                                query = "SELECT * FROM utente U join servizio_sanitario s on U.id=s.id WHERE mail = '" + email + "' AND password = '" + password + "'";

                                res1 = state1.executeQuery(query);

                                //L'utente è un servizio sanitario
                                //Setto i dati che ho ricavato in un oggetto ServizioSanitarioProvinciale
                                if (res1.next()) {
                                    ServizioSanitarioProvinciale user = new ServizioSanitarioProvinciale();
                                    user.setId(res1.getInt("id"));
                                    user.setEmail(res1.getString("mail"));
                                    user.setPassword(res1.getString("password"));
                                    user.setVia(res1.getString("via"));
                                    user.setCap(res1.getString("cap"));
                                    user.setCivico(res1.getString("civico"));
                                    user.setProvincia(res1.getString("provincia"));
                                    return user;
                                }
                            }

                        }
                    }

                }

            } catch (SQLException ex) {
                System.err.println("Sono entrato nel catch.........");
                System.err.println(ex);

                //return null;
            }
        } catch (SQLException ex) {

            throw new DAOException("Impossible to get the list of users", ex);

        }
        return null;
    }

    @Override
    public Utente getByEmail(String email) throws DAOException {
        String query;
        Utente user;
        try {
            Statement statement = CON.createStatement();
            ResultSet rs;
            query = "SELECT * FROM utente WHERE mail = '" + email + "'";
            try {
                rs = statement.executeQuery(query);
                if (rs.next()) {
                    user = new Utente();
                    user.setId(rs.getInt("id"));
                    user.setEmail(rs.getString("mail"));
                    return user;
                }
            } catch (Exception e) {
            }
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Returns the list of all the valid {@link User users} stored by the
     * storage system.
     *
     * @return the list of all the valid {@code users}.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Stefano Chirico
     * @since 1.0.0.190414
     */
    @Override
    public List<Utente> getAll() throws DAOException {
        List<Utente> users = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery("SELECT * FROM utente ORDER BY lastname")) {

                PreparedStatement shoppingListsStatement = CON.prepareStatement("SELECT count(*) FROM users_shopping_lists WHERE id_user = ?");

                /* while (rs.next()) {
                    Paziente user = new Utente();
                    user.setId(rs.getInt("id"));
                    user.setNome(rs.getString("nome"));
                    user.setCognome(rs.getString("cognome"));
                    user.setEmail(rs.getString("email"));
                    user.setPassword(rs.getString("password"));
                    user.setSesso(rs.getString("sesso"));
                    user.setCodiceFiscale(rs.getString("codice_fiscale"));
                    user.setFoto(rs.getString("foto"));
                    user.setDataDiNascita(rs.getString("data_di_nascita"));
                    user.setLuogoDiNascita(rs.getString("luogo_di_nascita"));
                    
                

                    shoppingListsStatement.setInt(1, user.getId());

                    ResultSet counter = shoppingListsStatement.executeQuery();
                    counter.next();
                    
                    users.add(user);
                }*/
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return users;
    }

    /**
     * Update the user passed as parameter and returns it.
     *
     * @param user the user used to update the persistence system.
     * @return the updated user.
     * @throws DAOException if an error occurred during the action.
     *
     * @author Stefano Chirico
     * @since 1.0.180413
     */
    @Override
    public Utente update(Utente user) throws DAOException {
        if (user == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed user is null"));
        }
        /*
        try (PreparedStatement std = CON.prepareStatement("UPDATE app.users SET email = ?, password = ?, name = ?, lastname = ?, avatar_path = ? WHERE id = ?")) {
            std.setString(1, user.getNome());
            std.setString(2, user.getCognome());
            std.setString(3, user.getEmail());
            std.setString(4,user.getPassword());
            std.setString(5, user.getSesso());
            std.setString(6, user.getCodiceFiscale());
            std.setString(7, user.getFoto());
            std.setString(8, user.getDataDiNascita());
            std.setString(9,user.getLuogoDiNascita());
            
            
            std.setInt(11, user.getId());// era 6
//------------------------------------------------------------------------------DA RIVEDERE------------------------------------------------------------------------------------------------
  
            
            if (std.executeUpdate() == 1) {
                return user;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }*/
        return user;//da togliere questa riga
    }

    @Override
    public boolean setPassword(int idUtente, String password) throws DAOException {

        boolean res = false;
        try {

            //ResultSet rs;
            try {
                PreparedStatement std = CON.prepareStatement("UPDATE UTENTE SET PASSWORD='" + password + "' WHERE ID=" + idUtente);
                res = true;
                std.executeUpdate();

            } catch (Exception e) {
                throw new DAOException("Impossible to update password", e);
            }
        } catch (Exception e) {
            throw new DAOException("Impossible to update password", e);
        }
        return res;
    }

    @Override
    public int getIdMedicoDiBase(String nome, String cognome, String via, String civico, String cap, String provincia) throws DAOException {

        String getIdQuery = "Select id from medico_di_base where nome='" + nome.toLowerCase() + "' and cognome='" + cognome.toLowerCase() + "' and via='" + via + "' and civico='" + civico + "' and cap='" + cap + "' and provincia='" + provincia + "'";
        int id = 0;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(getIdQuery)) {
                while (rs.next()) {
                    id = Integer.parseInt(rs.getString("id"));
                }

            }
        } catch (SQLException ex) {
            throw new DAOException("Impossibile trovare medico di base", ex);
        }

        return id;
    }

    @Override
    public int getByCodiceFiscale(String codiceFiscale) throws DAOException {
        String query = "SELECT ID FROM PAZIENTE WHERE CODICE_FISCALE='" + codiceFiscale + "'";
        int id = 0;
        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(query)) {
                while (rs.next()) {
                    id = Integer.parseInt(rs.getString("id"));
                }

            }
        } catch (SQLException ex) {
            throw new DAOException("Impossibile trovare il paziente", ex);
        }

        return id;
    }

    @Override
    public MedicoDiBase getMedicoDiBaseByPaziente(int idUtente) throws DAOException {
        String query="SELECT M.ID AS ID, M.NOME AS NOME, M.COGNOME as COGNOME FROM PARCO_PAZIENTI PAR JOIN MEDICO_DI_BASE M ON PAR.ID_MEDICO=M.ID WHERE DATA_FINE IS NULL AND PAR.ID_PAZIENTE = "+idUtente;
        try (Statement stm1 = CON.createStatement()) {
            try (ResultSet rs = stm1.executeQuery(query)) {
                    
                    MedicoDiBase medico= new MedicoDiBase();
                    while (rs.next()) {
                        
                        medico.setId(rs.getInt("id"));
                        medico.setNome(rs.getString("nome"));
                        medico.setCognome(rs.getString("cognome"));
                    }
                 return medico;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            MedicoDiBase med=new MedicoDiBase();
            med.setNome("Null");
            med.setCognome("Null");
            return med;
         }
    }

    @Override
    public int getIdServizioSanitario(String provincia) throws DAOException {
        String query="select id from servizio_sanitario where provincia='"+provincia+"'";
        try (Statement stm1 = CON.createStatement()) {
            try (ResultSet rs = stm1.executeQuery(query)) {
                    
                 while (rs.next()) {
                    return rs.getInt("id");
                 }
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            
            
         }
        return 0;
    }

    @Override
    public Long getCountPaziente() throws DAOException {
        
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM paziente");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }

        return 0L;

    }

    @Override
    public List pageBySearchValue(String searchValue, Long start, Long length) throws DAOException {
        /*if ((start == null) && (length == null)) {
            return findBySearchValue(searchValue);
        }*/
        
        //System.err.println("Qualcuno ha chiamato il DAO, start="+start+" lenght="+length);

        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM paziente p join utente u on p.id=u.id  WHERE lower(mail) like ? OR lower(nome) like ? OR lower(cognome) like ? OFFSET ? ROWS FETCH NEXT ? ROWS ONLY")) {
            stm.setString(1, "%"+searchValue+"%");
            stm.setString(2, "%"+searchValue+"%");
            stm.setString(3, "%"+searchValue+"%");
            stm.setLong(4, start);
            stm.setLong(5, length);
            try (ResultSet rs = stm.executeQuery()) {
                

                List<Paziente> users = new ArrayList<Paziente>();
                while (rs.next()) {
                    
                    Paziente user = new Paziente();
                    
                    user.setId(rs.getInt("id"));
                    user.setNome(rs.getString("nome"));
                    user.setCognome(rs.getString("cognome"));
                    user.setEmail(rs.getString("mail"));
                    user.setPassword(rs.getString("password"));
                    user.setSesso(rs.getString("sesso"));
                    user.setCodiceFiscale(rs.getString("codice_fiscale"));
                    user.setFoto(rs.getString("foto"));
                    user.setDataDiNascita(rs.getString("data_di_nascita"));
                    user.setLuogoDiNascita(rs.getString("luogo_di_nascita"));
                    user.setProvincia(rs.getString("provincia"));
                    
                    

                    

                    users.add(user);
                }

                return users;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            throw new DAOException("Impossible to get the list of users", ex);
        }

    }

    @Override
    public List getPazienteAutocomplete(String string) throws DAOException {
        //System.err.println("qualcuno ha chiamato il dao");
        String query = "select nome, cognome, codice_fiscale, data_di_nascita from paziente where nome LIKE '%"+string+"%' or cognome LIKE '%"+string+"%' ";

        try (PreparedStatement stm = CON.prepareStatement(query)) {
            
            try (ResultSet rs = stm.executeQuery()) {
                

                List<ObjectPerAutocompletamento> pazienti = new ArrayList<ObjectPerAutocompletamento>();
                int i=1;
                while (rs.next()) {
                    ObjectPerAutocompletamento o = new ObjectPerAutocompletamento();
                    o.setId(i);
                    o.setText(rs.getString("nome")+" "+rs.getString("cognome")+" "+rs.getString("data_di_nascita")+" "+rs.getString("codice_fiscale"));
                    //System.err.println("Ho trovato qualcosa");
                    
                    //pazienti.add(rs.getString("nome")+" "+rs.getString("cognome")+" "+rs.getString("data_di_nascita")+" "+rs.getString("codice_fiscale"));
                    pazienti.add(o);
                    i++;
                }
                //System.err.println(pazienti.size());
                return pazienti;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            throw new DAOException("Impossible to get the list of pazienti", ex);
        }
    }

    @Override
    public Paziente getPazienteById(int id) throws DAOException {
       
        String query="SELECT * FROM paziente WHERE id="+id;
        try (Statement stm1 = CON.createStatement()) {
            try (ResultSet rs = stm1.executeQuery(query)) {
                    
                    Paziente user = new Paziente();
                    while (rs.next()) {
                        
                        
                                user.setId(rs.getInt("id"));
                                user.setNome(rs.getString("nome"));
                                user.setCognome(rs.getString("cognome"));
                                
                                user.setSesso(rs.getString("sesso"));
                                user.setCodiceFiscale(rs.getString("codice_fiscale"));
                                user.setFoto(rs.getString("foto"));
                                user.setDataDiNascita(rs.getString("data_di_nascita"));
                                user.setLuogoDiNascita(rs.getString("luogo_di_nascita"));
                                user.setProvincia(rs.getString("provincia"));
                    }
                 return user;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            Paziente med=new Paziente();
            med.setNome("Null");
            med.setCognome("Null");
            return med;
         }
    }

    @Override
    public void setFotoPaziente(int id, String path) throws DAOException {
        
        try {

            //ResultSet rs;
            try {
                PreparedStatement std = CON.prepareStatement("UPDATE PAZIENTE SET FOTO='" + path + "' WHERE ID=" + id);
                
                std.executeUpdate();

            } catch (Exception e) {
                System.err.println("Errore"+e);
                throw new DAOException("Impossible to update password", e);
            }
        } catch (Exception e) {
            System.err.println("Errore"+e);
            throw new DAOException("Impossible to update password", e);
        }
        
    }

    @Override
    public String getPazienteMailById(int id) throws DAOException {
        String query="SELECT mail FROM paziente P inner join utente U on P.id = U.id WHERE U.id="+id;
        String email="";
        try (Statement stm1 = CON.createStatement()) {
            try (ResultSet rs = stm1.executeQuery(query)) {
                while (rs.next()) {   
                email=rs.getString("mail");
                }
            }
        } catch (SQLException ex) {
            System.err.println("Email non trovata "+ex);
         }
        return email;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    

    

    

    

}
