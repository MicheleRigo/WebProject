/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.EsamePazienteDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.MedicoSpecialistaDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.VisitePazienteDAO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author miche
 */
public class MediciSpecialistiServlet extends HttpServlet {

    private UserDAO userDao; 
    private MedicoSpecialistaDAO medicoSpecialistaDAO;
    private EsamePazienteDAO esamePazienteDAO;
    private VisitePazienteDAO visitePazienteDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            medicoSpecialistaDAO = daoFactory.getDAO((MedicoSpecialistaDAO.class));
            esamePazienteDAO= daoFactory.getDAO(EsamePazienteDAO.class);
            visitePazienteDAO = daoFactory.getDAO(VisitePazienteDAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        String provincia= request.getParameter("provincia");
        String specializzazione= request.getParameter("specializzazione");
        
        JSONObject json= new JSONObject();
        
        try {
            
            
            
            json= new JSONObject();
                    
                   json.put("listaMediciSpecialista", new JSONArray(medicoSpecialistaDAO.getMedicoSpecialistaByProvincia(provincia,specializzazione)));
                    
            //System.err.println(json);
                    
          
        } catch (DAOException ex) {
            Logger.getLogger(GetDatiPazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json.toString());
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codFisc= request.getParameter("codiceFiscalePaziente");
        String esame= request.getParameter("nomeEsame");
        String esito= request.getParameter("esito");
        String idMedico= request.getParameter("idMedico");
        
        try {
            int idPaziente = userDao.getByCodiceFiscale(codFisc);
            int idEsame = esamePazienteDAO.getIdEsameByName(esame);
            
            
            visitePazienteDAO.insertVisita(idPaziente, idEsame, esito, idMedico);
        } catch (DAOException ex) {
            Logger.getLogger(MediciSpecialistiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public void dispatch(javax.servlet.http.HttpServletRequest request,
        javax.servlet.http.HttpServletResponse response, String nextPage)
        throws ServletException, IOException {

        RequestDispatcher dispatch = request.getRequestDispatcher(nextPage);
        dispatch.forward(request, response);

}
}
