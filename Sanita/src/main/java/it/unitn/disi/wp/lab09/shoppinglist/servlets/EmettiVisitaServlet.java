/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.EsamePazienteDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.FarmaciDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.PrescrizioniDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;



/**
 *
 * @author matteo
 */
public class EmettiVisitaServlet extends HttpServlet {

    private UserDAO userDao;
    private FarmaciDAO farmacoDao;
    private EsamePazienteDAO esameDao;
    private PrescrizioniDAO prescrizioneDao;
    
    public void sendEmail(String destinatario){
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "ansebadeca@gmail.com";
        final String password = "dogauejvmqnomdiq";
        Properties props = System.getProperties();

        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");
        
         Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario, false));
            msg.setSubject("Nuova Prescrizione");
            msg.setText("E' per te disponibile una nuova prescrizione, puoi vederla nella sezione 'Prescrizioni' dopo aver effettuato il login");
            msg.setSentDate(new Date());

            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace(System.err);
        }


    }
       
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            farmacoDao = daoFactory.getDAO(FarmaciDAO.class);
            esameDao = daoFactory.getDAO(EsamePazienteDAO.class);
            prescrizioneDao = daoFactory.getDAO(PrescrizioniDAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        JSONObject json= new JSONObject();
        
        
        String cf = request.getParameter("codiceFiscalePaziente");
        String nome_farmaco = request.getParameter("nome_farmaco");
        String desc = request.getParameter("descrizione");
        int medico = Integer.parseInt(request.getParameter("id_medico"));
        int id, id_farmaco, id_esame;
        
        
        
        try {
            id = userDao.getByCodiceFiscale(cf);
            
            String email = userDao.getPazienteMailById(id);
            
            
            id_farmaco = farmacoDao.getIdFarmacoByName(nome_farmaco);
            
            
            prescrizioneDao.setFarmaco(id, id_farmaco, medico, desc);
            
            sendEmail(email);
            
            json= new JSONObject();
            json.putOpt("errore", "tuttoBene");
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json.toString());
                      
        } catch (DAOException ex) {
            json= new JSONObject();
            json.putOpt("errore", ex.toString());
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json.toString());
            
            Logger.getLogger(EmettiVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
