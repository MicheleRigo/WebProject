package it.unitn.disi.wp.lab09.shoppinglist.services;



import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.DatatablesResponse;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.05.19
 */
@Path("cercaPazienti2")
public class CercaPazienteService {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;
    
    @Context
    private HttpServletResponse response;

    private ServletContext servletContext;
    
    private UserDAO userDao;

    /**
     * Creates a new instance of LanguagesService
     */
    public CercaPazienteService() {
        
    }

    @Context
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
        if (servletContext != null) {
            DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
            if (daoFactory == null) {
                throw new RuntimeException(new ServletException("Impossible to get dao factory for storage system"));
            }
            try {
                userDao = daoFactory.getDAO(UserDAO.class);
            } catch (DAOFactoryException ex) {
                throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
            }
        }
    }

//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public String getU() {
//        return "{ \"users\": [1, 2, 3, 4] }";
//    }
    /**
     * Retrieves representation of an instance of
     * it.unitn.disi.wp.lab12.shoppinglist.services.UsersService
     *
     * @param searchValue
     * @param draw
     * @param start
     * @param length
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPazienti(@QueryParam("term") String queryTerm) throws DAOException, IOException {
       

        List<String> results = new ArrayList<String>();
        JSONObject json= new JSONObject();
        if (queryTerm == null) {
            results.add("");
        } else {
            try {
                json= new JSONObject();
                    json.putOpt("results", new JSONArray(userDao.getPazienteAutocomplete(queryTerm)));
                //results=userDao.getPazienteAutocomplete(queryTerm);
                
               
            } catch (Exception e) {
                System.err.println(e);
            }
            
            //System.err.println(results);
            Gson gson = new Gson();
            
            //return "{\"results\": [{\"id\": 1,\"text\":\"Option 1\"} {\"id\": 2,\"text\": \"Option 2\"}]}";
            
            return json.toString();
        }
        
        
        /*String string=JSON.toJSONString(results);
        System.err.println(string);
        //response.getWriter().write(string.toString());
        //return JSON.toJSONString(results);
        /*System.err.println(json.toString());
        return json.toString();*/
        

       //return "{\"results\": [{\"id\": 1,\"text\":\"Option 1\"} {\"id\": 2,\"text\": \"Option 2\"}]}";
       json.putOpt("results", new JSONArray());
       return json.toString();
    }

    /**
     * PUT method for updating or creating an instance of UsersService
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    

}
