/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 09 - Shopping List Implementation
 * UniTN
 */
package it.unitn.disi.wp.lab09.shoppinglist.servlets;

import com.google.common.hash.Hashing;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.CookieDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.dao.UserDAO;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoDiBase;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.MedicoSpecialista;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Paziente;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Utente;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet that handles the login web page.
 *
 * @author Stefano Chirico &lt;stefano dot chirico at unitn dot it&gt;
 * @since 2019.04.14
 */
public class LoginServlet extends HttpServlet {
    
    private UserDAO userDao;
    private CookieDAO cookieDAO;
    //private final HashMap<String, Integer> authenticatedUsers;

    public LoginServlet() {
        super();
    }
    
    private boolean authenticate(String username, String password) {
        return ((username != null) && (password != null));
    }


    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            cookieDAO=daoFactory.getDAO(CookieDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jSessionId = null;
        String contextPath = getServletContext().getContextPath();
        Cookie[] cookies = request.getCookies();
        if ((cookies != null) && (cookies.length > 0)) {
            // I have at least 2 cookies. I check is there are authentication
            // cookies (username and password)
            for (Cookie cookie : cookies) {
                switch (cookie.getName()) {
                    case "login":
                        jSessionId = cookie.getValue();
                        break;
                    /*case "prova":
                        System.err.println("Funziona");
                        break;*/
                }
            }
            try{
            Utente user=userDao.getByPrimaryKey(cookieDAO.getUtente(jSessionId));
            request.getSession().setAttribute("user", user);
            System.out.println("arrivato "+user.getId()+" "+user.getEmail());
            if(user instanceof Paziente){
                dispatch(request, response, "restricted/users.html");
            }
            if(user instanceof MedicoDiBase){
                dispatch(request, response, "restricted/medico.html");
                //response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/users.html?id=" + user.getId()));
            }
            if(user instanceof MedicoSpecialista){
                dispatch(request, response, "restricted/specialista.html");
                //response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/medicoSpecialista.html?id=" + user.getId()));
            }
            //response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/users.html?id=" + user.getId()));
            }catch (Exception e){
                response.sendRedirect("loginSanita.html");
            }
        }
        else{
            response.sendRedirect("loginSanita.html");
        }
    }

    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     * @author Stefano Chirico
     * @since 1.0.0.190407
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String email = request.getParameter("username");
        String password = request.getParameter("password");
        password=Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        ///888
        
        //Provo a cercare l'utente
        try {
            Utente user = userDao.getByEmailAndPassword(email, password);
            //Se non l'ho trovato rimando al login
            if (user == null) {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "loginSanita.html"));
            }else{//altrimenti metto user nelle variabile di sessione
                request.getSession().setAttribute("user", user);
                if(request.getParameterValues("ricordami")!=null){//se la checkbox è selezionata
                    String jSessionId = Long.toHexString(Double.doubleToLongBits(Math.random()));
                    Cookie cookie = new Cookie("login", jSessionId);
                    cookie.setMaxAge(60*60);
                    response.addCookie(cookie);
                    cookieDAO.addCookie(jSessionId, user.getId());
                }
                request.getSession().setAttribute("user", user);
                if(user instanceof Paziente){
                    dispatch(request, response, "users.jsp");
                }
                if(user instanceof MedicoDiBase){
                    dispatch(request, response, "medicoDiBase.jsp");
                    //response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/users.html?id=" + user.getId()));
                }
                if(user instanceof MedicoSpecialista){
                    dispatch(request, response, "medicoSpecialista.jsp");
                    //response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/medicoSpecialista.html?id=" + user.getId()));
                }
                if(user instanceof ServizioSanitarioProvinciale){
                    System.err.println(((ServizioSanitarioProvinciale) user).getProvincia());
                    dispatch(request, response, "servizioSanitarioProvinciale.jsp");
                    //response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/medicoSpecialista.html?id=" + user.getId()));
                    
                }
            }   
        } catch (DAOException ex) {

            request.getServletContext().log("Impossible to retrieve the user", ex);

        }
    }
    
    public void dispatch(javax.servlet.http.HttpServletRequest request,
        javax.servlet.http.HttpServletResponse response, String nextPage)
        throws ServletException, IOException {

        RequestDispatcher dispatch = request.getRequestDispatcher(nextPage);
        dispatch.forward(request, response);

}


}
