/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.DatiRicettaFarmaci;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Prescrizione;
import java.util.LinkedList;

/**
 *
 * @author miche
 */
public interface PrescrizioniDAO extends DAO<Prescrizione,Integer>{
    
    public LinkedList<Prescrizione> getAllPrescrizioniEsami(int idPaziente) throws DAOException;
    
    public LinkedList<Prescrizione> getAllPrescrizioniFarmaci(int idPaziente) throws DAOException;
    
    public Prescrizione getByID(int id) throws DAOException;
    
    public void setPrenotato(int id) throws DAOException;
    
    public DatiRicettaFarmaci getDatiRicetta(int id) throws DAOException;

    public void setFarmaco(int id, int id_farmaco, int medico, String desc) throws DAOException;

    public void setEsame(int id, int id_esame, int medico, String desc) throws DAOException;
    
    public void setVisita(int id, int medico, String desc) throws DAOException;
}
