/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.entities;

/**
 *
 * @author miche
 */
public class Prescrizione {
    
    private int id;
    private String descrizione;
    private String nomeMedico;
    private String cognomeMedico;
    private String nomePaziente;
    private String cognomePaziente;
    private String farmaco;
    private String esame;
    private int idEsame;
    private String codiceFiscalePaziente;
    private boolean visualizzato;
    private String dataPrescrizione;
    private int specializzazioneEsame;
    private boolean prenotato;

    public boolean isPrenotato() {
        return prenotato;
    }

    public void setPrenotato(boolean prenotato) {
        this.prenotato = prenotato;
    }
    

    public int getIdEsame() {
        return idEsame;
    }

    public void setIdEsame(int idEsame) {
        this.idEsame = idEsame;
    }

    public int getSpecializzazioneEsame() {
        return specializzazioneEsame;
    }

    public void setSpecializzazioneEsame(int specializzazioneEsame) {
        this.specializzazioneEsame = specializzazioneEsame;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getNomeMedico() {
        return nomeMedico;
    }

    public void setNomeMedico(String nomeMedico) {
        this.nomeMedico = nomeMedico;
    }

    public String getCognomeMedico() {
        return cognomeMedico;
    }

    public void setCognomeMedico(String cognomeMedico) {
        this.cognomeMedico = cognomeMedico;
    }

    public String getNomePaziente() {
        return nomePaziente;
    }

    public void setNomePaziente(String nomePaziente) {
        this.nomePaziente = nomePaziente;
    }

    public String getCognomePaziente() {
        return cognomePaziente;
    }

    public void setCognomePaziente(String cognomePaziente) {
        this.cognomePaziente = cognomePaziente;
    }

    public String getFarmaco() {
        return farmaco;
    }

    public void setFarmaco(String farmaco) {
        this.farmaco = farmaco;
    }

    public String getEsame() {
        return esame;
    }

    public void setEsame(String esame) {
        this.esame = esame;
    }

    public boolean isVisualizzato() {
        return visualizzato;
    }

    public String getCodiceFiscalePaziente() {
        return codiceFiscalePaziente;
    }

    public void setVisualizzato(boolean visualizzato) {
        this.visualizzato = visualizzato;
    }

    public String getDataPrescrizione() {
        return dataPrescrizione;
    }

    public void setDataPrescrizione(String dataPrescrizione) {
        this.dataPrescrizione = dataPrescrizione;
    }

    public void setCodiceFiscalePaziente(String codiceFiscalePaziente) {
        this.codiceFiscalePaziente = codiceFiscalePaziente;
    }
    
    
    
    
}
