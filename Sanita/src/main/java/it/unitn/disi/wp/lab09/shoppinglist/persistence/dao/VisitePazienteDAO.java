/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.lab09.shoppinglist.persistence.dao;

import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.lab09.shoppinglist.persistence.entities.Visita;
import java.util.LinkedList;

/**
 *
 * @author miche
 */
public interface VisitePazienteDAO extends DAO<Visita, Integer>{
    
    public LinkedList<Visita> getVisitePaziente(int idPaziente) throws DAOException;
    
    public void insertVisita(int idPaziente, int idEsame, String esito, String idMedico) throws DAOException;
}
